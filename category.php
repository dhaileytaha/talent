<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/coconut-talent-logo.png">
    <!-- Title -->
    <title>Coconut Talent </title>
    <link href="assets/css/theme.css" rel="stylesheet"><!-- Main Template Styles -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]-->
    <script src="assets/js/html5shiv.min.js" type="text/javascript"></script>
    <!--[endif]-->
    <style>
        body{
            background-color: white!important;
        }
    </style>
</head>
<head>
    <?php
    include ('header.php');
    ?>
    <style>
        .menu-dark #navbar-container a{
            color : green!important;
        }
    </style>
</head>
<body class="skin-light menu-transparent menu-logo-centered menu-dark menu-logo-overflow menu-hover-default is-home-header wpb-js-composer">

<div id="top"></div>
<a id="top-arrow" class="scroll" href="#top"></a>
<div id="loading-overlay">
    <div id="loader">
        <div class="loader8">
            <div class="loader8-container loader8-container1">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container2">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container3">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
        </div>
    </div>
</div>
<?php
include('nav.php');
?>

<div class="top hidden-xs hidden-sm">
</div>
<div class="belowTop">	
</div>
<div class="site-container">
    <div id="page" class="hfeed site pusher">
        <div id="page-content">
            <!-- START Trainers block -->
            <div class="tm-block tm-block-bottom-a tm-section-light tm-section-padding-large" style="top:200px">
                <div class="uk-container uk-container-center">
                    <section class="tm-bottom-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1">
                            <!-- START Trainers intro text block -->
                            <!--<div class="uk-panel">
                                <h3 class="uk-panel-title uk-text-center">Categories</h3>
                                <p class="uk-text-large uk-text-center ak-trainers">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie mi massa. Etiam lacinia lorem rhoncus consequat molestie. Fusce maximus nisi vel nisi lobortis placerat. In tempor ex in tellus accumsan congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                            </div>-->
                            <!-- END Trainers intro text block -->

                            <!-- START Trainers foto block -->
                            <div class="uk-panel">
                                <div class="category-module-trainers ">
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 1 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/munmun.jpg');">
                                                        <img src="assets/img/image4.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Mohini Mishra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 1 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 2 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/image2.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Falguni Pathak</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 2 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 3 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/image12.jpeg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Dipna Patel</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 3 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 4 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/keerthi.jpg" class="" width="295" height="500" alt="" style="color:black!important">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Keerthi Sagathia</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Singer Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 4 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 5 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/shantanu.jpg');">
                                                        <img src="assets/img/trainer-4.jpg" class="uk-invisible" width="295" height="500" alt="Amelie Noble">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Shantanu Maheshwari</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 5 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 6 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/munmun.jpg');">
                                                        <img src="assets/img/trainer-3.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Munmun Dutta</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 6 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 7 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/shahid.jpg');">
                                                        <img src="assets/img/trainer-2.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Shahid Kapoor</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 7 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 8 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/priyanka.jpg');">
                                                        <img src="assets/img/trainer-1.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Priyanka Chopra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 8 block -->
                                        </div>
                                    </div>
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 9 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/deepika.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Deepika Podukone</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 9 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 10 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/priyanka.jpg" class="" style="" width="295" height="500" alt="" >
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Priyanka Chopra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 10 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 11 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/ranveer.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Ranveer Singh</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 11 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 12 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/farhan.jpg" class="" style="height: 285px" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Farhan Akhtar</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 12 block -->
                                        </div>
                                    </div>
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 13 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/munmun.jpg');">
                                                        <img src="assets/img/image2.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Mohini Mishra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 13 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 14 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('images/demo/trainers/trainer-7.jpg');">
                                                        <img src="assets/img/ranveer.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Ranveer Singh</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 14 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 15 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/image12.jpeg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Dipna Patel</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 15 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 16 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/keerthi.jpg" class="" width="295" height="500" alt="" style="height:295px!important">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Keerthi Sagathia</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Singer Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 16 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 17 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/shantanu.jpg');">
                                                        <img src="assets/img/trainer-4.jpg" class="uk-invisible" width="295" height="500" alt="Amelie Noble">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Shantanu Maheshwari</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 17 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 18 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/munmun.jpg');">
                                                        <img src="assets/img/trainer-3.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Munmun Dutta</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 18 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 19 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/shahid.jpg');">
                                                        <img src="assets/img/trainer-2.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Shahid Kapoor</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 19 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left uk-grid-margin">
                                            <!-- START Trainer 19 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('assets/img/priyanka.jpg');">
                                                        <img src="assets/img/trainer-1.jpg" class="uk-invisible" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden; height:298px">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Priyanka Chopra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 19 block -->
                                        </div>
                                    </div>
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 20 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('images/demo/trainers/trainer-8.jpg');">
                                                        <img src="assets/img/deepika.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Deepika Podukone</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 20 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 21 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/image4.jpg" class="" style="height: 285px" width="295" height="500" alt="Edward Lane">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Mohini Mishra</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Female
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 21 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 22 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('images/demo/trainers/trainer-6.jpg');">
                                                        <img src="assets/img/ranveer.jpg" class="" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Ranveer Singh</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 22 block -->
                                        </div>
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 23 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel" >
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('');">
                                                        <img src="assets/img/farhan.jpg" class="" style="height: 285px" width="295" height="500" alt="">
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="portfolio.php">Farhan Akhtar</a></h4>
                                                                <div class="tm-panel-subtitle">
                                                                    Actor Male
                                                                </div>
                                                                <div class='theme-socials-container text-center'>
                                                                    <a href='#' title='facebook' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='twitter' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                    <a href='#' title='instagram' target='_blank' class='wolf-social-link'>
                                                                        <span class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span>
                                                                    </a>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 23 block -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Trainers foto block -->
                        </div>
                    </section>
                </div>
            </div>
            <!-- END Trainers block -->
            <div style="height:100px"></div>
        </div><!-- .site-wrapper -->
    </div><!-- #main -->
</div><!-- #page-container -->
<?php
include('footer.php');
?>


    </div><!-- #page .hfeed .site -->
</div><!-- .site-container -->

<script type='text/javascript'
        src='assets/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript' src='assets/js/scriptsa94e.js?ver=4.4.1'></script>
<script type='text/javascript' src='assets/js/instagram370e.js?ver=1.4.3'></script>
<script type='text/javascript'
        src='assets/js/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='assets/js/woocommerce.min4468.js?ver=2.5.5'></script>
<script type='text/javascript'
        src='assets/js/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'
        src='assets/js/cart-fragments.min4468.js?ver=2.5.5'></script>
<script type='text/javascript' src='assets/js/mediaelement-and-player.min7796.js?ver=2.18.1-a'></script>
<script type='text/javascript' src='assets/js/wp-mediaelement.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/jquery.swipebox.minecc3.js?ver=1.3.0.2'></script>
<script type='text/javascript' src='assets/js/isotope.pkgd.min7406.js?ver=2.0.1'></script>
<script type='text/javascript' src='assets/js/imagesloaded.pkgd.min3a79.js?ver=3.1.8'></script>
<script type='text/javascript' src='assets/js/wow.minf269.js?ver=1.0.1'></script>
<script type='text/javascript' src='assets/js/waypoints.mina752.js?ver=4.11.2.1'></script>
<script type='text/javascript' src='assets/js/jquery.flexslider.min605a.js?ver=2.2.2'></script>
<script type='text/javascript' src='assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.haParallax8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.counterup.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.memo.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.fittext62ea.js?ver=1.2'></script>
<script type='text/javascript' src='assets/js/jquery.wolfSlider2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.carousels2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.likesnviews2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/youtube-video-bg2846.js?ver=1.5.5'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var WolfThemeParams = {
        "ajaxUrl": "http://demo.wolfthemes.com/tattoopro/wp-admin/admin-ajax.php",
        "siteUrl": "http://demo.wolfthemes.com/tattoopro/",
        "accentColor": "",
        "headerPercent": "85",
        "breakPoint": "1140",
        "lightbox": "swipebox",
        "videoLightbox": null,
        "footerUncover": null,
        "headerUncover": null,
        "sliderEffect": null,
        "sliderAutoplay": null,
        "sliderSpeed": null,
        "sliderPause": null,
        "infiniteScroll": "true",
        "infiniteScrollMsg": "Loading...",
        "infiniteScrollEndMsg": "No more post to load",
        "loadMoreMsg": "Load More",
        "infiniteScrollEmptyLoad": "assets/img/empty.gif",
        "newsletterPlaceholder": "Your email",
        "isHomeSlider": null,
        "heroFadeWhileScroll": "true",
        "heroParallax": "1",
        "homeHeaderType": "video",
        "isHome": "1",
        "blogWidth": "boxed",
        "menuPosition": "logo-centered",
        "modernMenu": "",
        "currentPostType": [],
        "enableParallaxOnMobile": null,
        "enableAnimationOnMobile": null,
        "doPageTransition": "1",
        "doBackToTopAnimation": "1",
        "onePageMenu": "",
        "isOnePageOtherPage": "1",
        "isStickyMenu": "true",
        "addMenuType": "side",
        "workType": null,
        "isTopbar": null,
        "menuStyle": "transparent",
        "years": "Years",
        "months": "Months",
        "weeks": "Weeks",
        "days": "Days",
        "hours": "Hours",
        "minutes": "Minutes",
        "seconds": "Seconds",
        "replyTitle": "Share your thoughts",
        "doWoocommerceLightbox": "",
        "leftMenuTransparency": null,
        "layout": "wide",
        "HomeHeaderVideoBgType": "selfhosted"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='assets/js/jquery.functions2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/comment-reply.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/wp-embed.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/js_composer_front.mina752.js?ver=4.11.2.1'></script>
</body>

</html>
