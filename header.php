<?php
/**
 * Created by PhpStorm.
 * User: Red Cherrt 13
 * Date: 08-06-2016
 * Time: 03:17 PM
 */
?>
<!DOCTYPE html>
<head>
   
    <link rel='stylesheet' id='swipebox-css' href='assets/css/swipebox.min6f3e.css?ver=1.3.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='dashicons-css' href='assets/css/dashicons.min3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='normalize-css' href='assets/css/normalize6e0e.css?ver=3.0.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bagpakk-css'
          href='assets/css/bagpakk-wordpress-custom.min8a54.css?ver=1.0.0' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='flexslider-css' href='assets/css/flexslider3601.css?ver=2.2.0'
          type='text/css' media='all'/>
    <!-- <link rel='stylesheet' id='owlcarousel-css'
          href='assets/css/owl.carousel.min001e.css?ver=2.0.0' type='text/css' media='all'/> -->
    <link rel='stylesheet' id='icon-pack-css' href='assets/css/icon-pack.min2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='icon-pack-css' href='assets/css/icon.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tattoopro-style-css' href='assets/css/main2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tattoopro-default-css' href='assets/css/style2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <!--[if lte IE 8]-->
    <link rel='stylesheet' id='tattoopro-ie8-style-css'
          href='http://demo.wolfthemes.com/tattoopro/wp-content/themes/tattoopro/css/ie8.css?ver=4.5.2' type='text/css'
          media='all'/>
    <!--[endif]-->
    <link rel='stylesheet' id='contact-form-7-css'
          href='assets/css/stylesa94e.css?ver=4.4.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-twitter-css'
          href='assets/css/twitter.min6e0e.css?ver=3.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='assets/css/settings6619.css?ver=5.2.5' type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-gram-css' href='assets/css/instagram370e.css?ver=1.4.3'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-theme-google-fonts-css'
          href='http://fonts.googleapis.com/css?family=Merriweather:400,700|Montserrat:400,700|Roboto:400,700|Raleway:400,700&amp;subset=latin,latin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='assets/css/js_composer.mina752.css?ver=4.11.2.1' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='wolf-widgets-pack-css'
          href='assets/css/widgets.min4bf4.css?ver=1.0.3' type='text/css'
          media='all'/>
    <link rel="stylesheet" href="assets/css/style.css">
    <script type='text/javascript' src='assets/js/jquerycad0.js?ver=1.12.3'></script>
    <script type='text/javascript' src='assets/js/jquery-migrate.min2fca.js?ver=1.4.0'></script>
    <script type='text/javascript'
            src='assets/js/jquery.themepunch.tools.min6619.js?ver=5.2.5'></script>
    <script type='text/javascript'
            src='assets/js/jquery.themepunch.revolution.min6619.js?ver=5.2.5'></script>
    <script type='text/javascript'
            src='assets/js/add-to-cart.min4468.js?ver=2.5.5'></script>
    <script type='text/javascript'
            src='assets/js/woocommerce-add-to-carta752.js?ver=4.11.2.1'></script>
    <script type='text/javascript' src='assets/js/modernizrf7ff.js?ver=2.8.3'></script>

    <style type="text/css"></style>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]-->
    <link rel="stylesheet" type="text/css"
          href="http://demo.wolfthemes.com/tattoopro/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
          media="screen"><!--[endif]--><!--[if IE  8]-->
    <link rel="stylesheet" type="text/css"
          href="http://demo.wolfthemes.com/tattoopro/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css"
          media="screen"><!--[endif]-->
</head>
