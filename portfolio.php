<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/coconut-talent-logo.png">
    <!-- Title -->
    <title>Coconut Talent </title>
    <link href="assets/css/theme.css" rel="stylesheet"><!-- Main Template Styles -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    <link href="assets/css/owl.carousel.min.css">
    <link href="assets/css/owl.theme.default.min.css">
    
    <script src="assets/js/owl.carousel.min.js"></script>
    <!--[if lt IE 9]-->
    <script src="assets/js/html5shiv.min.js" type="text/javascript"></script>
    <!--[endif]-->

    <style>
        body{
            background-color:white !important;
/*
            overflow-y: hidden!important;
*/
        }
        .menu-transparent.has-header-image.show-title-area #navbar-container, .menu-transparent.is-home-header #navbar-container,
        .menu-semi-transparent.has-header-image.show-title-area #navbar-container,
        .menu-semi-transparent.is-home-header #navbar-container {
            position: absolute;
            background-color:white !important;
        }
        .menu-dark #navbar-container a {
            color:green !important;
        }
        .sticky-menu.menu-dark.menu-semi-transparent.has-header-image.show-title-area #navbar-container, .sticky-menu.menu-dark.menu-semi-transparent.is-home-header #navbar-container, .sticky-menu.menu-dark.menu-transparent.has-header-image.show-title-area #navbar-container, .sticky-menu.menu-dark.menu-transparent.is-home-header #navbar-container{
            color: white !important;
        }
        hr{
            margin-top: 1rem!important;
        }
        .description{
            padding: 0px 0px 0px 46px;
        }
        h3{
            font-size: 20px!important;
        }
        h5{
            font-size: 17px!important;
        }
        p{
            font-size: 15px!important;
        }
        #parent {
        display: flex;
        }
        #narrow {
            width: 100px !important;
            /* Just so it's visible */
        }
        #wide {
            flex: 1;
            /* Grow to rest of container */
            /* Just so it's visible */
        }
        .actorInSearch{
            width: 125px!important;
            color: black!important;
        }
        .fluid-video{
            padding-bottom: 280px!important;
            width: 360px!important;
            float:left;
            margin-right: 50px;
        }
    </style>
</head>
<head>
    <link rel='stylesheet' id='siteorigin-panels-front-css'
          href='assets/css/frontad2f.css?ver=2.4.8' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontawesome-css'
          href='assets/css/fontawesome/style3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='talents_css_page_widgets-css'
          href='assets/css/page_widgets3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='css_widgets_animation-css'
          href='assets/css/animate.min3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='swpm.common-css'
          href='assets/css/swpm.common3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='validationEngine.jquery-css'
          href='assets/css/validationEngine.jquery3428.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='css_font_awesome-css'
          href='assets/css/font-awesome6aec.css?ver=3.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='css_prettyphoto-css'
          href='assets/css/prettyPhoto3428.css?ver=4.5.2' type='text/css' media='all'/>
    <link rel='stylesheet' id='css_portfolio-css' href='assets/css/portfolio3428.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='talents-style-css' href='assets/css/style3428.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='sliders-css' href='assets/css/sliders5152.css?ver=1.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='typography-css' href='assets/css/typography3428.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='widgets-css' href='assets/css/widgets.min3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='layout-css' href='assets/css/layout3428.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='css_responsive-css'
          href='assets  /css/responsive3428.css?ver=4.5.2' type='text/css' media='all'/>
    <!--[if lt IE 10]-->
    <link rel='stylesheet' id='ie9_down-css'
          href='http://kayapati.com/demos/talents/wp-content/themes/talents/css/ie9_down.css?ver=4.5.2' type='text/css'
          media='all'/>
    <!--[endif]-->
    <link rel='stylesheet' id='google-font-family-css'
          href='http://fonts.googleapis.com/css?family=Cambo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CRopa+Sans:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CExo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CCousine:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7COpen+Sans:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CLora:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%26subset%3Dlatin%2Clatin-ext&amp;subset=latin,latin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='google-frame-border-font-family-css'
          href='http://fonts.googleapis.com/css?family=Open+Sans:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7C&amp;subset=latin,latin-ext'
          type='text/css' media='all'/>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wppath = {"template_path": "http:\/\/kayapati.com\/demos\/talents\/wp-content\/themes\/talents"};
        var kaya_ajax_url = {"ajaxurl": "http:\/\/kayapati.com\/demos\/talents\/wp-admin\/admin-ajax.php"};
        var cpath = {"plugin_dir": "http:\/\/kayapati.com\/demos\/talents\/wp-content\/plugins\/kaya-talents-plugin"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='assets/js/jquerycad01.js'></script>
    <script type='text/javascript' src='assets/js/jquery-migrate.min2fca1.js'></script>
    <style type="text/css">
        #sidebar .widget_container ul li {
            border-color: #cccccc;
        }

        .widget_container .tagcloud a {
            background-color: #f2f2f2;
            border-color: #8DC73F;
            color: #333333;
        }

        .widget_container .tagcloud a:hover {
            background-color: #333333;
            color: #f2f2f2;
        }

        .portfolio_content_info_section {
            background: #ffffff !important;
        }

        .portfolio_content_info_section span.pf_content_show {
            border-bottom: 30px solid #ffffff !important;
            color: #333333;
        }

        #mid_container_wrapper .pf_model_info_wrapper ul li, .portfolio_content_info_section span.pf_content_show i {
            color: #333333;
        }

        #mid_container_wrapper .pf_model_info_wrapper ul li span {
            color: #969696;
        }

        .pf_model_info_wrapper {
            border-color: #4a4a4a;
        }

        .pf_tab_list > ul > li.pf_tabs_style, .pf_tab_list > ul > li.pf_tabs_style:first-child::before, .pf_tab_list > ul > li.pf_tabs_style:last-child::before, .model_share_icon {
            background: #f2f2f2;
            color: #7a7a7a;
            border: 1px solid #e5e5e5;
        }

        .pf_tab_list > ul > li.pf_tabs_style > a {
            color: #7a7a7a !important;
        }

        .pf_tab_list > ul > li.pf_tabs_style.tab-active, .pf_tab_list > ul > li.pf_tabs_style:hover, .model_info_active, .pf_back_to_page a, .model_info_icon, a.user_profile_button {
            background: #8DC73F;
            color: #ffffff !important;
            border: 1px solid #8DC73F;
        }

        .pf_tab_list > ul > li.pf_tabs_style.tab-active a, .pf_tab_list > ul > li.pf_tabs_style:hover a {
            color: #ffffff !important;
        }

        .social_media_sharing_icons span.share_on_title {
            color: #eaeaea;
            background: #645f5a;
        }

        . .mid_container_wrapper_section .social_media_sharing_icons li a {
            color: #ffffff !important;
            background: #8DC73F;
        }

        .mid_container_wrapper_section .social_media_sharing_icons li {
            border-color: #b9b9b9 !important;
        }

        #gallery_horizontal .owl-next, #gallery_horizontal .owl-prev {
            color: #ffffff;
            background: #8DC73F;
        }

        .ytp-large-play-button.ytp-touch-device .ytp-large-play-button-bg, .ytp-cued-thumbnail-overlay:hover .ytp-large-play-button-bg {
            fill: #ffffff;
        }

        #model_description {
        }

        #model_description .description h1, #model_description .description h2, #model_description .description h3, #model_description .description h4, #model_description .description h5, #model_description .description h6, #model_description .description h3 span {
            color: #333333 !important;;
        }

        #model_description .description p, #model_description .description ul li, #model_description .description, #model_description .description p span {
            color: #787878 !important;;
        }

        #model_description .description a {
            color: #333333 !important;;
        }

        #model_description .description a:hover {;
        }

        .pf_taxonomy_gallery.portfolio_content_wrapper span.pf_title_wrapper {
            color: #151515;
        }

        .pf_taxonomy_gallery.portfolio_content_wrapper span.pf_title_wrapper {
            background: #ffffff;
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            letter-spacing: 0px;
        }

        .pf_taxonomy_gallery .pf_title_description {
            background: #8DC73F;
        }

        .pf_taxonomy_gallery .pf_title_description h3 {
            color: #151515 !important;
        }

        .pf_taxonomy_gallery .pf_title_description span {
            color: #ffffff !important;
        }

        .pf_taxonomy_gallery .pf_title_description a {
            color: #ffffff !important;
            background-color: #8DC73F !important;
            border: 1px solid #8DC73F !important;
        }

        .pf_taxonomy_gallery .pf_title_description a:hover {
            color: #ffffff !important;
            background-color: #8DC73F !important;
            border-color: #8DC73F !important;
        }

        .pf_social_share_icons li a, .user_send_email_post {
            background-color: #f2f2f2;
            color: #333333 !important;
        }

        .user_send_email_post input:not(.user_profile_button), i.user_email_form_close {
            color: #333333 !important;
        }

        .pf_social_share_icons li a {
            border-color: #eeeeee;
        }

        .pf_social_share_icons ul li a:hover {
            background-color: #8DC73F !important;
            color: #ffffff !important;
        }
        .menu-logo-centered.menu-logo-overflow .logo a img{
            max-height:150px !important;
        }
    </style>




    <link rel='stylesheet' id='swipebox-css' href='assets/css/swipebox.min6f3e.css?ver=1.3.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='dashicons-css' href='assets/css/dashicons.min3428.css?ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='normalize-css' href='assets/css/normalize6e0e.css?ver=3.0.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bagpakk-css'
          href='assets/css/bagpakk-wordpress-custom.min8a54.css?ver=1.0.0' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='flexslider-css' href='assets/css/flexslider3601.css?ver=2.2.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='owlcarousel-css'
          href='assets/css/owl.carousel.min001e.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='icon-pack-css' href='assets/css/icon-pack.min2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='icon-pack-css' href='assets/css/icon.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tattoopro-style-css' href='assets/css/main2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tattoopro-default-css' href='assets/css/style2846.css?ver=1.5.5'
          type='text/css' media='all'/>
    <!--[if lte IE 8]-->
    <link rel='stylesheet' id='tattoopro-ie8-style-css'
          href='http://demo.wolfthemes.com/tattoopro/wp-content/themes/tattoopro/css/ie8.css?ver=4.5.2' type='text/css'
          media='all'/>
    <!--[endif]-->
    <link rel='stylesheet' id='contact-form-7-css'
          href='assets/css/stylesa94e.css?ver=4.4.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-twitter-css'
          href='assets/css/twitter.min6e0e.css?ver=3.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='assets/css/settings6619.css?ver=5.2.5' type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-gram-css' href='assets/css/instagram370e.css?ver=1.4.3'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wolf-theme-google-fonts-css'
          href='http://fonts.googleapis.com/css?family=Merriweather:400,700|Montserrat:400,700|Roboto:400,700|Raleway:400,700&amp;subset=latin,latin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='assets/css/js_composer.mina752.css?ver=4.11.2.1' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='wolf-widgets-pack-css'
          href='assets/css/widgets.min4bf4.css?ver=1.0.3' type='text/css'
          media='all'/>
    <link rel="stylesheet" href="assets/css/style.css">
    <script type='text/javascript' src='assets/js/jquerycad0.js?ver=1.12.3'></script>
    <script type='text/javascript' src='assets/js/jquery-migrate.min2fca.js?ver=1.4.0'></script>
    <script type='text/javascript'
            src='assets/js/jquery.themepunch.tools.min6619.js?ver=5.2.5'></script>
    <script type='text/javascript'
            src='assets/js/jquery.themepunch.revolution.min6619.js?ver=5.2.5'></script>
    <script type='text/javascript'
            src='assets/js/add-to-cart.min4468.js?ver=2.5.5'></script>
    <script type='text/javascript'
            src='assets/js/woocommerce-add-to-carta752.js?ver=4.11.2.1'></script>
    <script type='text/javascript' src='assets/js/modernizrf7ff.js?ver=2.8.3'></script>
    <link rel='stylesheet' id='woocommerce-general-css'  href='assets/css/woocommerce4468.css?ver=2.5.5' type='text/css' media='all' />

    <!--[if lte IE 9]-->
    <link rel="stylesheet" type="text/css"
          href="http://demo.wolfthemes.com/tattoopro/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
          media="screen"><!--[endif]--><!--[if IE  8]-->
    <link rel="stylesheet" type="text/css"
          href="http://demo.wolfthemes.com/tattoopro/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css"
          media="screen"><!--[endif]-->
</head>
<body class="skin-light menu-transparent menu-logo-centered menu-dark menu-logo-overflow menu-hover-default is-home-header wpb-js-composer">

<div id="top"></div>
<a id="top-arrow" class="scroll" href="#top"></a>
<div id="loading-overlay">
    <div id="loader">
        <div class="loader8">
            <div class="loader8-container loader8-container1">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container2">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container3">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: Red Cherrt 13
 * Date: 08-06-2016
 * Time: 11:11 AM
 */
?>
<head>
    <style>
        #categorySubmenu {

        }
    </style>
</head>
<div id="ceiling">
    <div id="navbar-container" class="clearfix">
        <div class="wrap">
            <div id="navbar-left" class="navbar clearfix">
                <nav class="site-navigation-primary navigation main-navigation clearfix" role="navigation">
                    <div class="menu-main-menu-left-container">
                        <ul id="menu-main-menu-left" class="nav-menu">
                            <li id="menu-item-180"
                                class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-has-children menu-item-180 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="index.php">Home</a></span>
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-347"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-347 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="indexfe9e.html?home_header_type=video"><span>Video Background</span></a>
                                    </li>
                                    <li id="menu-item-346"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-346 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="index8f32.html?home_header_type=revslider&amp;header_revslider=slider1"><span>Slider Revolution</span></a>
                                    </li>
                                    <li id="menu-item-345"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-345 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="index577c.html?home_header_type=standard"><span>Image Zoom Effect</span></a>
                                    </li>
                                </ul>-->
                            </li>
                            <li id="menu-item-181"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-181 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="#">Artists</a></span>
                                <ul class="sub-menu" id="categorySubmenu">
                                    <li id="menu-item-356"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Models</span></a>
                                    </li>
                                    <li id="menu-item-355"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Actors</span></a>
                                    </li>
                                    <li id="menu-item-354"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-354 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Singers</span></a>
                                    </li>
                                    <li id="menu-item-353"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="category.php"><span>Dancers</span></a>
                                    </li>
                                    <li id="menu-item-353"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="category.php"><span>Directors</span></a>
                                    </li>
                                </ul>
                            </li>
                          <!--  <li id="menu-item-182"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-182 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="">Artist</a></span>-->
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-186"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index1216.html?blog_type=masonry"><span>Masonry</span></a></li>
                                    <li id="menu-item-183"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7b2d.html?blog_type=large"><span>Large</span></a></li>
                                    <li id="menu-item-184"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7bb3.html?blog_type=sidebar"><span>Sidebar</span></a></li>
                                    <li id="menu-item-185"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index4304.html?blog_type=sided"><span>Medium Image</span></a>
                                    </li>
                                    <li id="menu-item-187"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-187 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7761.html?blog_type=masonry&amp;blog_width=wide&amp;blog_infinite_scroll_trigger="><span>Pinterest Style</span></a>
                                    </li>
                                    <li id="menu-item-188"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-188 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index1082.html?blog_type=grid"><span>Grid</span></a></li>
                                    <li id="menu-item-189"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-189 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="#"><span>Single Post Layouts</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-190"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-190 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/standard-post/index.php"><span>Post with Sidebar</span></a>
                                            </li>
                                            <li id="menu-item-191"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-191 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/image-post/index.php"><span>Full Width Post</span></a>
                                            </li>
                                            <li id="menu-item-192"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-192 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/gallery-post/index.php"><span>Splitted Post</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>-->
                           <!-- </li>-->
                            <li id="menu-item-193"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-193 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="albums/index.php"><span>About</span></a>
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-195"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-195 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/index.php"><span>Vertical</span></a></li>
                                    <li id="menu-item-194"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/indexb958.html?gallery_type=modern"><span>Horizontal</span></a>
                                    </li>
                                    <li id="menu-item-196"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-196 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/indexe548.html?gallery_type=grid&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid</span></a>
                                    </li>
                                    <li id="menu-item-197"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-197 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/index8f9d.html?gallery_type=grid&amp;gallery_width=wide&amp;gallery_cols=4&amp;gallery_padding=no-padding&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid Full Width</span></a>
                                    </li>
                                    <li id="menu-item-198"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-198 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="#"><span>Single Gallery Layouts</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-199"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-199 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/mosaic/index.php"><span>Mosaic</span></a></li>
                                            <li id="menu-item-200"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-200 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/masonry/index.php"><span>Masonry</span></a></li>
                                            <li id="menu-item-201"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-201 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/standard-grid/index.php"><span>Standard Grid</span></a>
                                            </li>
                                            <li id="menu-item-202"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-202 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/squares-grid/index.php"><span>Squares Grid</span></a>
                                            </li>
                                            <li id="menu-item-203"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-203 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/4-columns-full-width/index.php"><span>4 Columns Full Width</span></a>
                                            </li>
                                            <li id="menu-item-204"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-204 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/2-columns-centered/index.php"><span>2 Columns Centered</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>-->
                            </li>
                        </ul>
                    </div>
                </nav><!-- #site-navigation-primary -->
            </div><!-- #navbar -->
            <div class="logo"><a href="" rel="home"><img class="logo-dark"
                                                                  src="assets/img/coconut-talent-logo.png"
                                                                  alt=""><img class="logo-light"
                                                                              style="height: 150px;"
                                                                              src="assets/img/coconut-talent-logo.png"
                                                                              alt="Tattoo Pro"></a></div>
            <div id="navbar-right" class="navbar clearfix">
                <nav class="site-navigation-primary navigation main-navigation clearfix" role="navigation">
                    <span class="menu-main-menu-right-container">
                        <ul id="menu-main-menu-right" class="nav-menu">
                            <li id="menu-item-281"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-281 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="rfp.php">Contact</a></span>
                               <!-- <ul class="sub-menu">
                                    <li id="menu-item-298"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-298 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Templates</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-299"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="small-width/index.php"><span>Small Width</span></a></li>
                                            <li id="menu-item-300"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="full-width/index.php"><span>Full Width</span></a></li>
                                            <li id="menu-item-301"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="sidebar-at-left/index.php"><span>Sidebar at Left</span></a>
                                            </li>
                                            <li id="menu-item-302"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-302 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="sidebar-at-right/index.php"><span>Sidebar at Right</span></a>
                                            </li>
                                            <li id="menu-item-303"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-303 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="page-with-comments/index.php"><span>Page with Comments</span></a>
                                            </li>
                                            <li id="menu-item-304"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a href="post-archives/index.php"><span>Post Archives</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-305"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-305 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Headers</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-306"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-306 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-parallax/index.php"><span>Header Parallax</span></a>
                                            </li>
                                            <li id="menu-item-307"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-zoom/index.php"><span>Header Zoom</span></a></li>
                                            <li id="menu-item-308"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-video/index.php"><span>Header Video</span></a></li>
                                            <li id="menu-item-309"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-full-screen/index.php"><span>Header Full Screen</span></a>
                                            </li>
                                            <li id="menu-item-310"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-small-height/index.php"><span>Header Small Height</span></a>
                                            </li>
                                            <li id="menu-item-311"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-breadcrumb/index.php"><span>Header Breadcrumb</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-312"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-312 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Menu Styles</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-318"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-318 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/index1e9f.html?menu_position=default"><span>Menu Logo at Left</span></a>
                                            </li>
                                            <li id="menu-item-313"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-313 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexc639.html?menu_width=wide&amp;menu_position=default"><span>Menu Wide</span></a>
                                            </li>
                                            <li id="menu-item-314"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-314 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexcd08.html?menu_style=semi-transparent"><span>Menu Semi-Transparent</span></a>
                                            </li>
                                            <li id="menu-item-315"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-315 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/index1974.html?menu_style=plain"><span>Menu Plain</span></a>
                                            </li>
                                            <li id="menu-item-316"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-316 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexccaf.html?menu_skin=light&amp;menu_style=plain"><span>Menu Light</span></a>
                                            </li>
                                            <li id="menu-item-317"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-317 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexade1.html?menu_position=left&amp;search_menu_item=0&amp;cart_menu_item=0"><span>Menu at Left</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-319"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-319 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Socials</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-320"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="instagram/index.php"><span>Instagram</span></a></li>
                                            <li id="menu-item-321"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a href="twitter/index.php"><span>Twitter</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-344"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="faq/index.php"><span>FAQ</span></a></li>
                                    <li id="menu-item-322"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="contact/index.php"><span>Contact</span></a></li>
                                    <li id="menu-item-323"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="maintenance/index.php"><span>Maintenance</span></a>
                                    </li>
                                    <li id="menu-item-324"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-324 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="404"><span>404</span></a></li>
                                </ul>-->
                            </li>
                            <li id="menu-item-282"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>I'm lookin for</span></a>
                                <!-- <ul class="sub-menu">
                                     <li id="menu-item-283"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="parallax/index.php"><span><i
                                             class="fa fa-align-center"></i>Parallax</span></a></li>
                                     <li id="menu-item-284"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="assets/img/index.php"><span><i
                                             class="fa fa-picture-o"></i>Image Galleries</span></a></li>
                                     <li id="menu-item-285"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="image-sliders/index.php"><span><i
                                             class="fa fa-eye"></i>Image Sliders</span></a></li>
                                     <li id="menu-item-286"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-286 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="team-members/index.php"><span><i
                                             class="fa fa-users"></i>Team Members</span></a></li>
                                     <li id="menu-item-287"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="socials/index.php"><span><i
                                             class="fa fa-share-alt"></i>Socials</span></a></li>
                                     <li id="menu-item-288"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-288 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="blocks/index.php"><span><i
                                             class="fa fa-square"></i>Blocks</span></a></li>
                                     <li id="menu-item-289"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="buttons/index.php"><span><i
                                             class="fa fa-square-o"></i>Buttons</span></a></li>
                                     <li id="menu-item-290"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a
                                             href="call-to-action/index.php"><span><i
                                             class="fa fa-exclamation-circle"></i>Call to Action</span></a></li>
                                     <li id="menu-item-291"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="custom-fonts/index.php"><span><i
                                             class="fa fa-font"></i>Custom Fonts</span></a></li>
                                     <li id="menu-item-292"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="counters/index.php"><span><i
                                             class="fa fa-sort-alpha-asc"></i>Counters</span></a></li>
                                     <li id="menu-item-293"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="count-down/index.php"><span><i
                                             class="fa fa-sort-alpha-desc"></i>Count Down</span></a></li>
                                     <li id="menu-item-294"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-294 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="notifications/index.php"><span><i
                                             class="fa fa-exclamation"></i>Notifications</span></a></li>
                                     <li id="menu-item-295"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-295 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a
                                             href="accordions-toggles/index.php"><span><i class="fa fa-toggle-on"></i>Accordions &#038; Toggles</span></a>
                                     </li>
                                     <li id="menu-item-296"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="tabs/index.php"><span><i
                                             class="fa fa-table"></i>Tabs</span></a></li>
                                     <li id="menu-item-297"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="columns/index.php"><span><i
                                             class="fa fa-columns"></i>Columns</span></a></li>
                                 </ul>-->
                            </li>
                            <li id="menu-item-282"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Blogs</span></a>
                            </li>
                            <!--<li class="socials-menu-item">-->
                               <!-- <div class='theme-socials-container text-center'><a href='#' title='facebook'
                                                                                    target='_blank'
                                                                                    class='wolf-social-link'><span
                                            class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='twitter' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='instagram' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='foursquare' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social ti-foursquare span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a>
                                </div>--><!-- .theme-socials-container -->
                            <!--</li>-->
                            <script>
                                $(document).ready(function() {
                                    $('.parent').on("click",function(){

                                        $(this).find(".sub-menu").toggle();
                                        $(this).siblings().find(".sub-menu").hide();

                                        if($(".sub-menu:visible").length === 0 ){
                                            $(".child-menu").hide();
                                        }else {
                                            $(".child-menu").show();
                                        }
                                    });

                                    $(".sub-menu").on("click",function(){
                                        $(".sub-nav").hide();
                                        $(this).hide();
                                    });
                                });
                            </script>
                            <li id="menu-item-22"
                                class="parent menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span class="fa fa-search"></span></a>
                                 <ul class="sub-menu child-menu dropdown-menu mega-dropdown-menu row" style="background-color: whitesmoke !important; margin-top: 0px; width: 600px!important; margin-left: -380px">
                                     <li class="col-sm-5">
                                         <ul class="searchOptions">
                                             <li class="dropdown-header">New Artists</li>
                                             <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                 <div class="carousel-inner">
                                                     <div class="item active">
                                                         <a href="#"><img src="/assets/img/1.jpg" class="img-responsive" alt="product 1"></a>
                                                         <h4><small>Brad Pitt</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">49,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                     <div class="item">
                                                         <a href="#"><img src="/assets/img/2.jpg" class="img-responsive" alt="product 2"></a>
                                                         <h4><small>Josef Peter</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">9,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                     <div class="item">
                                                         <a href="#"><img src="/assets/img/3.jpg" class="img-responsive" alt="product 3"></a>
                                                         <h4><small>Tom Cruise</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">49,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                 </div>
                                                 <!-- End Carousel Inner -->
                                             </div>
                                             <!-- /.carousel -->
                                             <li class="divider"></li>
                                             <li><a href="category.php" style="color:black!important">View All <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                                         </ul>
                                     </li>
                                     <li class="col-sm-7">
                                         <ul>
                                             <li class="dropdown-header">Search</li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Actors :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Singers :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Models :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Dancers :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Directors :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li><a href="#">
                                                     <select class="" data-mega-menu-bg-repeat='no-repeat'>
                                                         <option selected disabled>Height</option>
                                                         <option>Less than 3 feet</option>
                                                         <option>3.1'</option>
                                                         <option>3.2'</option>
                                                         <option>3.3'</option>
                                                         <option>3.4'</option>
                                                         <option>3.5'</option>
                                                         <option>3.6'</option>
                                                         <option>3.7'</option>
                                                         <option>3.8'</option>
                                                         <option>3.9'</option>
                                                         <option>3.10'</option>
                                                         <option>3.11'</option>
                                                         <option>3.12'</option>
                                                         <option>4.1'</option>
                                                         <option>4.2'</option>
                                                         <option>4.3'</option>
                                                         <option>4.4'</option>
                                                         <option>4.5'</option>
                                                         <option>4.6'</option>
                                                         <option>4.7'</option>
                                                         <option>4.8'</option>
                                                         <option>4.9'</option>
                                                         <option>4.10'</option>
                                                         <option>4.11'</option>
                                                         <option>4.12'</option>
                                                         <option>5.1'</option>
                                                         <option>5.2'</option>
                                                         <option>5.3'</option>
                                                         <option>5.4'</option>
                                                         <option>5.5'</option>
                                                         <option>5.6'</option>
                                                         <option>5.7'</option>
                                                         <option>5.8'</option>
                                                         <option>5.9'</option>
                                                         <option>5.10'</option>
                                                         <option>5.11'</option>
                                                         <option>5.12'</option>
                                                         <option>Above 6</option>
                                                     </select>
                                                 </a>
                                             </li>
                                             <li class="divider"></li>
                                             <!--<li class="dropdown-header">Tops</li>-->
                                             <li><a href="category.php" class="btn btn-success" role="button"><button class="btn btn-success">Go</button></a></li>
                                         </ul>
                                     </li>
                                     <!--<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                         data-mega-menu-bg-repeat='no-repeat'><input type="search" placeholder="Search" style="width: 270px">
                                     </li>-->
                                     <!--<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                         data-mega-menu-bg-repeat='no-repeat' style="color:white; font-size: 16px">
                                         Category :<br>
                                         Actor Male
                                         <input type="checkbox" name="actor">
                                         Actor Female
                                         <input type="checkbox" name="actor"><br>
                                         Singer Male
                                         <input type="checkbox" name="actor">
                                         Singer Female
                                         <input type="checkbox" name="actor"><br>
                                         Model Male
                                         <input type="checkbox" name="actor">
                                         Model Female
                                         <input type="checkbox" name="actor"><br><br>
                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                  data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Gender</option>
                                             <option>Male</option>
                                             <option>Female</option>
                                         </select>

                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                 data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Height</option>
                                             <option>Less than 3 feet</option>
                                             <option>3.1'</option>
                                             <option>3.2'</option>
                                             <option>3.3'</option>
                                             <option>3.4'</option>
                                             <option>3.5'</option>
                                             <option>3.6'</option>
                                             <option>3.7'</option>
                                             <option>3.8'</option>
                                             <option>3.9'</option>
                                             <option>3.10'</option>
                                             <option>3.11'</option>
                                             <option>3.12'</option>
                                             <option>4.1'</option>
                                             <option>4.2'</option>
                                             <option>4.3'</option>
                                             <option>4.4'</option>
                                             <option>4.5'</option>
                                             <option>4.6'</option>
                                             <option>4.7'</option>
                                             <option>4.8'</option>
                                             <option>4.9'</option>
                                             <option>4.10'</option>
                                             <option>4.11'</option>
                                             <option>4.12'</option>
                                             <option>5.1'</option>
                                             <option>5.2'</option>
                                             <option>5.3'</option>
                                             <option>5.4'</option>
                                             <option>5.5'</option>
                                             <option>5.6'</option>
                                             <option>5.7'</option>
                                             <option>5.8'</option>
                                             <option>5.9'</option>
                                             <option>5.10'</option>
                                             <option>5.11'</option>
                                             <option>5.12'</option>
                                             <option>Above 6</option>
                                         </select>
                                         <br>
                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                 data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Age</option>
                                             <option>1</option>
                                             <option>2</option>
                                             <option>3</option>
                                             <option>4</option>
                                             <option>5</option>
                                             <option>6</option>
                                             <option>7</option>
                                             <option>8</option>
                                             <option>9</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>Above 20</option>
                                             <option>Below 35</option>
                                             <option>Above 35</option>
                                         </select>
                                         <br><br>
                                         <button class="btn btn-success pull-right">Go</button><br>
                                     </li>
                                 </ul>
                            </li>-->
                            <!--<li class="menu-item">

                                    <input type="search" placeholder="Search" style="width: 170px">

                            </li>-->
                        </ul>
                            </li>
                            </ul>
                    </div>
                </nav><!-- #site-navigation-primary --> 
            </div><!-- #navbar -->
        </div><!-- .wrap -->
    </div><!-- #navbar-container -->
</div><!--#Ceiling-->

<div id="mobile-bar" class="clearfix">
    <div id="mobile-bar-inner">
        <div id="menu-toggle" class="menu-toggle">
            <div class="burger-before"></div>
            <div class="burger"></div>
            <div class="burger-after"></div>
        </div>
        <div class="logo"><a href="index.php" rel="home"><img class="logo-dark"
                                                              src=""
                                                              alt=""><img class="logo-light"
                                                                                    src=""
                                                                                    alt=""></a></div>
    </div>
</div>
<div id="navbar-mobile-container">
    <div id="navbar-mobile" class="navbar clearfix">
        <!-- <span id="close-menu">&times;</span> -->
        <nav id="site-navigation-primary-mobile" class="navigation main-navigation clearfix" role="navigation">

            <div class="menu-main-menu-left-container">
                <ul id="mobile-menu" class="nav-menu dropdown">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-180 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="index.php"><span>Home</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-347 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="indexfe9e.html?home_header_type=video"><span>Video Background</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-346 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="index8f32.html?home_header_type=revslider&amp;header_revslider=slider1"><span>Slider Revolution</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-345 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="index577c.html?home_header_type=standard"><span>Image Zoom Effect</span></a>
                            </li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-181 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="portfolio.php"><span>Category</span></a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Models</span></a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Actor</span></a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-354 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="#"><span>Singer</span></a></li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Artist</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index1216.html?blog_type=masonry"><span>Masonry</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7b2d.html?blog_type=large"><span>Large</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7bb3.html?blog_type=sidebar"><span>Sidebar</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index4304.html?blog_type=sided"><span>Medium Image</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-187 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7761.html?blog_type=masonry&amp;blog_width=wide&amp;blog_infinite_scroll_trigger="><span>Pinterest Style</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-188 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="blog/index1082.html?blog_type=grid"><span>Grid</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-189 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Single Post Layouts</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-190 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="2015/05/31/standard-post/index.php"><span>Post with Sidebar</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-191 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="2015/05/31/image-post/index.php"><span>Full Width Post</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-192 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="2015/05/31/gallery-post/index.php"><span>Splitted Post</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-193 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>About</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-195 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/index.php"><span>Vertical</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/indexb958.html?gallery_type=modern"><span>Horizontal</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-196 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/indexe548.html?gallery_type=grid&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-197 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/index8f9d.html?gallery_type=grid&amp;gallery_width=wide&amp;gallery_cols=4&amp;gallery_padding=no-padding&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid Full Width</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-198 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Single Gallery Layouts</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-199 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/mosaic/index.php"><span>Mosaic</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-200 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/masonry/index.php"><span>Masonry</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-201 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/standard-grid/index.php"><span>Standard Grid</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-202 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/squares-grid/index.php"><span>Squares Grid</span></a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-203 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/4-columns-full-width/index.php"><span>4 Columns Full Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-204 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/2-columns-centered/index.php"><span>2 Columns Centered</span></a>
                                    </li>
                                </ul>-->
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu-main-menu-right-container">
                <ul id="mobile-menu" class="nav-menu dropdown">
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-281 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="contact.php"><span>Contact</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-298 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Templates</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="small-width/index.php"><span>Small Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="full-width/index.php"><span>Full Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="sidebar-at-left/index.php"><span>Sidebar at Left</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-302 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="sidebar-at-right/index.php"><span>Sidebar at Right</span></a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-303 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="page-with-comments/index.php"><span>Page with Comments</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="post-archives/index.php"><span>Post Archives</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-305 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Headers</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-306 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-parallax/index.php"><span>Header Parallax</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-zoom/index.php"><span>Header Zoom</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-video/index.php"><span>Header Video</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-full-screen/index.php"><span>Header Full Screen</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-small-height/index.php"><span>Header Small Height</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-breadcrumb/index.php"><span>Header Breadcrumb</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-312 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Menu Styles</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-318 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/index1e9f.html?menu_position=default"><span>Menu Logo at Left</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-313 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexc639.html?menu_width=wide&amp;menu_position=default"><span>Menu Wide</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-314 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexcd08.html?menu_style=semi-transparent"><span>Menu Semi-Transparent</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-315 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/index1974.html?menu_style=plain"><span>Menu Plain</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-316 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexccaf.html?menu_skin=light&amp;menu_style=plain"><span>Menu Light</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-317 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexade1.html?menu_position=left&amp;search_menu_item=0&amp;cart_menu_item=0"><span>Menu at Left</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-319 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Socials</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="instagram/index.php"><span>Instagram</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="twitter/index.php"><span>Twitter</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="faq/index.php"><span>FAQ</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="contact/index.php"><span>Contact</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="maintenance/index.php"><span>Maintenance</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-324 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="404"><span>404</span></a></li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>RFP</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="parallax/index.php"><span><i
                                            class="fa fa-align-center"></i>Parallax</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="assets/img/index.php"><span><i
                                            class="fa fa-picture-o"></i>Image Galleries</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="image-sliders/index.php"><span><i
                                            class="fa fa-eye"></i>Image Sliders</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-286 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="team-members/index.php"><span><i
                                            class="fa fa-users"></i>Team Members</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="socials/index.php"><span><i
                                            class="fa fa-share-alt"></i>Socials</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-288 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="blocks/index.php"><span><i
                                            class="fa fa-square"></i>Blocks</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="buttons/index.php"><span><i
                                            class="fa fa-square-o"></i>Buttons</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="call-to-action/index.php"><span><i
                                            class="fa fa-exclamation-circle"></i>Call to Action</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="custom-fonts/index.php"><span><i
                                            class="fa fa-font"></i>Custom Fonts</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="counters/index.php"><span><i
                                            class="fa fa-sort-alpha-asc"></i>Counters</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="count-down/index.php"><span><i
                                            class="fa fa-sort-alpha-desc"></i>Count Down</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-294 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="notifications/index.php"><span><i
                                            class="fa fa-exclamation"></i>Notifications</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-295 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="accordions-toggles/index.php"><span><i
                                            class="fa fa-toggle-on"></i>Accordions &#038; Toggles</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="tabs/index.php"><span><i
                                            class="fa fa-table"></i>Tabs</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="columns/index.php"><span><i
                                            class="fa fa-columns"></i>Columns</span></a></li>
                        </ul>-->
                    </li>
                    <li class="socials-menu-item">

                        <!--<div class='theme-socials-container text-center'><a href='#' title='facebook' target='_blank'
                                                                            class='wolf-social-link'><span
                                    class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='twitter' target='_blank' class='wolf-social-link'><span
                                    class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='instagram' target='_blank' class='wolf-social-link'><span
                                    class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='foursquare' target='_blank' class='wolf-social-link'><!--<span
                                    class='wolf-social ti-foursquare span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a>
                        </div>-->
                    </li>
                </ul>
            </div>
        </nav><!-- #site-navigation-primary -->
    </div><!-- #navbar -->
</div>

<div class="top hidden-xs hidden-sm">
</div>
<div class="belowTop">
</div>
<div class="site-container">
    <div id="page" class="hfeed site pusher">
        <div id="page-content">
            <!-- Start Middle Section  -->
            <div id="mid_container_wrapper" class="mid_container_wrapper_section">
                <div id="mid_container" class="">
                    <div class="entry-content">
                        <div class="single_page_content_wrapper">
                            <div id="post-83"
                                 class="portfolio_main_content_wrapper item  post-83 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio_category-male-models portfolio_category-models">
                                <!-- Start Portfolio -->
                                <div class="pf_tabs_content_wrapper">
                                    <div id="pf_image_gallery" class="pf_tab_content">
                                        <!-- <div class="single_page_slider_loader"></div>
                                        <div id="gallery_horizontal" class="gallery_horizontal clearfix " data-column="5"> -->
                                            <div class="owl-carousel owl-theme">
                                                <div class="item"> <img class='' src='assets/img/5.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/1.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/2.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/3.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/4.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/1.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/2.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/4.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/3.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/5.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/1.jpg' /></div>
                                                <div class="item"><img class='' src='assets/img/2.jpg' /></div>
                                            </div>

                                            <!-- <div class="horizontal_item">
                                                <a href='#' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/5.jpg'/>
                                                </a>
                                            </div>
                                            <div class="horizontal_item">
                                                <a href='#' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/1.jpg' width=''  height='' style='height:477px!important' />
                                                </a>
                                            </div>
                                            <div class="horizontal_item">
                                                <a href='#' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/2.jpg' alt='girl-model-9' width=''  height='' style='height:477px!important' />
                                                </a>
                                            </div>
                                            <div class="horizontal_item">
                                                <a href='#' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/3.jpg' alt='girl-model-8' width=''  height='' style='height:477px!important' />
                                                </a>
                                            </div>
                                            <div class="horizontal_item">
                                                <a href='#' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/4.jpg' alt='girl-model-7' />
                                                </a>
                                            </div>
                                            <div class="horizontal_item">
                                                <a href='' data-gal='prettyPhoto[gallery_slider]' >
                                                    <img class='' src='assets/img/5.jpg' alt='girl-model-6' width=''  height='' style='height:477px!important' />
                                                </a>
                                            </div>-->
                                       <!--  </div>
                                    </div> -->
                                    </div>
                                    <div id="pf_tab2" class="pf_tab_content">
                                        <div id="gallery_horizontal"
                                             class="pf_enable_gallery_images gallery_images_column3 clearfix " data-column="3"><a
                                                href='#'
                                                title='portfolio-29' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-29-650x500_t.jpg'
                                                title='portfolio-27' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-27-650x500_t.jpg'
                                                                                                          alt='portfolio-27'/></a><a
                                                href='#'
                                                title='portfolio-25' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-25-650x500_t.jpg'
                                                                                                          alt='portfolio-25'/></a><a
                                                href='#'
                                                title='portfolio-23' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-23-650x500_t.jpg'
                                                                                                          alt='portfolio-23'/></a><a
                                                href='#'
                                                title='portfolio-21' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-21-650x500_t.jpg'
                                                                                                          alt='portfolio-21'/></a><a
                                                href='#'
                                                title='portfolio-19' data-gal='prettyPhoto[gallery]'><img class=''
                                                                                                          src='assets/img/portfolio-19-650x500_t.jpg'
                                                                                                          alt='portfolio-19'/></a>
                                        </div>
                                    </div>
                                    <div id="video_iframes" class="pf_tab_content">
                                        <div class="single_video_iframe gallery_video_column3 clearfix ">
                                             <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/m8R1aC8zzyY?showinfo=0;"></iframe>
                                            <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/D_kKm6TXr0Q?showinfo=0;"></iframe>
                                            <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/F1p_31ATL6k?showinfo=0;"></iframe>
                                            <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/hkpeaPgwtQU?showinfo=0;"></iframe>
                                            <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/CoravFzbF4Y?showinfo=0;"></iframe>
                                            <iframe width="100%" height="280" style="height:280px!important; width:380px!important" src="https://www.youtube.com/embed/N5KYFgYJZNE?showinfo=0;"></iframe>                                        </div>
                                    </div>
                                    <div id="model_description" class="pf_tab_content">
                                        <div class="one_third">
                                            <div class="description"><h3 class="title_style2">SHORT DESCRIPTION </h3>
                                                <img class='' src='assets/img/portfolio-21-650x500_t.jpg' alt='9' width=''
                                                    height='' style=''/>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="description"><h3 class="title_style2">EXPRIENCE</h3>
                                                <h5>2014</h5><hr>
                                                <h5>Gucci Refines a Nostalgia Now</h5>

                                                <p>Aliquam mattis iaculis nisl nec finibus. Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit.</p>
                                                <h5>2013</h5><hr>
                                                <h5>Faucibus orci luctus et</h5>
                                                <p>Aliquam mattis iaculis nisl nec finibus. Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit.</p>
                                                <h5>2012</h5><hr>
                                                <h5>Duis quis nisl sit amet</h5>
                                                <p>Aliquam mattis iaculis nisl nec finibus. Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit.</p>
                                            </div>
                                        </div>
                                        <!--<div class="one_third">
                                            <div class="description"><h3 class="title_style2">SKILLS AND QUALITIES</h3>
                                                <p>Donec convallis quam vehicula sapien venenatis volutpat.Aliquam mattis iaculis
                                                    nisl nec finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                                                <ul>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; A pleasant,
                                                        professional attitude with good ‘people skills’
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Good time-keeping
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Patience, stamina and
                                                        fitness to cope with long, tiring days.
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; The ability to cope
                                                        with criticism and rejection
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Good grooming and
                                                        willingness to look after yourself
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Fashion sense and
                                                        awareness of trends
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Good coordination
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; Confidence,
                                                        self-reliance and discipline
                                                    </li>
                                                    <li><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp; The ability to cope
                                                        with criticism and rejection
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div> <!-- End Portfolio Right Section -->

                        <div class="portfolio_content_info_section"><span class="pf_content_show pf_details_open">
                                <!--<img src="assets/img/n1.png" style="">-->
                                <i class="fa fa-minus"></i></span>
                            <div class="portfolio_content_info_wrapper">
                                <div class="container" style="padding:30px;">
                                    <div class="pf_model_info_wrapper">
                                        <ul>
                                            <li><strong>Name : </strong><span>Chris Walsh</span></li>
                                            <li><strong>Gender : </strong><span>Male</span></li>
                                            <li><strong>Age : </strong><span>30</span></li>
                                            <li><strong>Country : </strong><span>London</span></li>
                                            <li><strong>Height : </strong><span>5'10 (177 cm)</span></li>
                                            <li><strong>Chest / Bust : </strong><span>69cm / 27'2</span></li>
                                            <li><strong>Waist : </strong><span>74cm / 29'1</span></li>
                                            <li><strong>Hair Color : </strong><span>Dark Blonde</span></li>
                                            <li><strong>Eye Color : </strong><span>Green</span></li>
                                            <li><strong>Shoe Size : </strong><span>44 - 10</span></li>
                                            <li><strong>Categories : </strong><span>Models</span></li>
                                        </ul>
                                    </div>
                                    <div class="pf_tab_list pf_tabs_contant_wrapper">
                                        <ul>
                                            <li class="pf_tabs_style"><a href="#pf_image_gallery">PORTFOLIO</a></li>
                                            <li class="pf_tabs_style"><a href="#pf_tab2">POLARIOD</a></li>
                                            <li class="pf_tabs_style"><a href="#video_iframes">VIDEOS</a></li>
                                            <li class="pf_tabs_style"><a href="#model_description">BIOGRAPHY</a></li>
                                        </ul>
                                    </div>
                        <span class="pf_back_to_page"><a href="" title="Download Portfolio"><i
                                    class="fa fa-download"> </i></a></span>
                                    <div class="pf_social_share_icons">
                                        <ul>
                                            <li class="social_sharing_icons"><a
                                                    href="http://facebook.com/sharer.php?u=http://kayapati.com/demos/talents/model/chris-walsh/&amp;t=Chris%20Walsh"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                    title="Share on Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li class="social_sharing_icons"><a
                                                    href="http://twitter.com/home/?status=Chris%20Walsh%20-%20http://kayapati.com/demos/talents/model/chris-walsh/"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                    title="Tweet this!"><i class="fa fa-twitter"></i></a></li>
                                            <li class="social_sharing_icons"><a
                                                    href="index75bf.html?url=http://kayapati.com/demos/talents/model/chris-walsh/&amp;0=//plus.google.com/share"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
                                                        class="fa fa-google-plus"></i></a></li>
                                            <li class="social_sharing_icons"><a
                                                    href="http://linkedin.com/shareArticle?mini=true&amp;title=Chris%20Walsh&amp;url=http://kayapati.com/demos/talents/model/chris-walsh/"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                    title="Share on LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="social_sharing_icons"><a
                                                    href="../../../../../www.pinterest.com/join/indexf1c6.html?url=http://kayapati.com/demos/talents/model/chris-walsh/&amp;media=http://kayapati.com/demos/talents/wp-content/uploads/sites/97/2015/08/5.jpg"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
                                                        class="fa fa-pinterest-p"></i></a></li>
                                            <!-- <li class="social_sharing_icons"><a
                                                    href="http://stumbleupon.com/submit?url=http://kayapati.com/demos/talents/model/chris-walsh/&amp;title=Chris%20Walsh"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                    title="Stumble it"><i class="fa fa-stumbleupon"></i></a></li>
                                            <li class="social_sharing_icons"><a
                                                    href="http://digg.com/submit?url=http://kayapati.com/demos/talents/model/chris-walsh/&amp;title=Chris%20Walsh"
                                                    onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                    title="Digg this!"><i class="fa fa-digg"></i></a></li> -->
                                            <li class="social_sharing_icons "><a href="#" class="user_email_form"><i
                                                        class="fa fa-envelope"></i></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="user_send_email_post">
                                <i class="user_email_form_close fa fa-close"></i>
                                <div class="success_result" style="display:none;"></div>
                                <input type="hidden" name="user_post_link" id="user_post_link" value="index.html"/>
                                <p><input type="text" name="user_email" id="user_email" value="" placeholder="Your Email Address"/>
                                </p>
                                <p><input type="text" name="user_name" id="user_name" value="" placeholder="Your Name"/></p>
                                <p><input type="text" name="receiver_email" id="receiver_email" value=""
                                          placeholder="Send to Email Address"/></p>
                                <input type="submit" id="send_mail_to_post" class="user_profile_button" value="SUBMIT"/><img
                                    class="mail_form_load_img" style="display:none"
                                    src="../../wp-content/themes/talents/images/ajax-loader.gif" class=""/>
                            </div>
                        </div>
                    </div>    <!-- Portfolio End -->
                </div>
            </div>
        </div> <!-- end middle content section -->
        <!-- end middle Container wrapper section -->
        <script>
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })
        </script>
        <script type='text/javascript'
                src='assets/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
        <script type='text/javascript'
                src='assets/js/scriptsc1f9.js?ver=4.4.2'></script>

        <script type='text/javascript' src='assets/js/jquery.easing.1.33428.js?ver=4.5.2'></script>
        <script type='text/javascript' src='assets/js/jquery.prettyPhoto3428.js?ver=4.5.2'></script>
        <script type='text/javascript' src='assets/js/owl.carouselfbd4.js?ver=1.29'></script>
        <script type='text/javascript' src='assets/js/custom3428.js?ver=4.5.2'></script>

        </div><!-- .site-wrapper -->
    </div><!-- #main -->
</div><!-- #page-container -->
<?php
//include('footer.php');
?>

<script type='text/javascript'
        src='assets/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript' src='assets/js/scriptsa94e.js?ver=4.4.1'></script>
<script type='text/javascript' src='assets/js/instagram370e.js?ver=1.4.3'></script>
<script type='text/javascript'
        src='assets/js/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='assets/js/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'
        src='assets/js/cart-fragments.min4468.js?ver=2.5.5'></script>
<script type='text/javascript' src='assets/js/mediaelement-and-player.min7796.js?ver=2.18.1-a'></script>
<script type='text/javascript' src='assets/js/wp-mediaelement.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/jquery.swipebox.minecc3.js?ver=1.3.0.2'></script>
<script type='text/javascript' src='assets/js/isotope.pkgd.min7406.js?ver=2.0.1'></script>
<script type='text/javascript' src='assets/js/imagesloaded.pkgd.min3a79.js?ver=3.1.8'></script>
<script type='text/javascript' src='assets/js/wow.minf269.js?ver=1.0.1'></script>
<script type='text/javascript' src='assets/js/waypoints.mina752.js?ver=4.11.2.1'></script>
<script type='text/javascript' src='assets/js/jquery.flexslider.min605a.js?ver=2.2.2'></script>
<script type='text/javascript' src='assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.haParallax8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.counterup.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.memo.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.fittext62ea.js?ver=1.2'></script>
<script type='text/javascript' src='assets/js/jquery.wolfSlider2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.carousels2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.likesnviews2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/youtube-video-bg2846.js?ver=1.5.5'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var WolfThemeParams = {
        "ajaxUrl": "http://demo.wolfthemes.com/tattoopro/wp-admin/admin-ajax.php",
        "siteUrl": "http://demo.wolfthemes.com/tattoopro/",
        "accentColor": "",
        "headerPercent": "85",
        "breakPoint": "1140",
        "lightbox": "swipebox",
        "videoLightbox": null,
        "footerUncover": null,
        "headerUncover": null,
        "sliderEffect": null,
        "sliderAutoplay": null,
        "sliderSpeed": null,
        "sliderPause": null,
        "infiniteScroll": "true",
        "infiniteScrollMsg": "Loading...",
        "infiniteScrollEndMsg": "No more post to load",
        "loadMoreMsg": "Load More",
        "infiniteScrollEmptyLoad": "assets/img/empty.gif",
        "newsletterPlaceholder": "Your email",
        "isHomeSlider": null,
        "heroFadeWhileScroll": "true",
        "heroParallax": "1",
        "homeHeaderType": "video",
        "isHome": "1",
        "blogWidth": "boxed",
        "menuPosition": "logo-centered",
        "modernMenu": "",
        "currentPostType": [],
        "enableParallaxOnMobile": null,
        "enableAnimationOnMobile": null,
        "doPageTransition": "1",
        "doBackToTopAnimation": "1",
        "onePageMenu": "",
        "isOnePageOtherPage": "1",
        "isStickyMenu": "true",
        "addMenuType": "side",
        "workType": null,
        "isTopbar": null,
        "menuStyle": "transparent",
        "years": "Years",
        "months": "Months",
        "weeks": "Weeks",
        "days": "Days",
        "hours": "Hours",
        "minutes": "Minutes",
        "seconds": "Seconds",
        "replyTitle": "Share your thoughts",
        "doWoocommerceLightbox": "",
        "leftMenuTransparency": null,
        "layout": "wide",
        "HomeHeaderVideoBgType": "selfhosted"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='assets/js/jquery.functions2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/comment-reply.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/wp-embed.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/js_composer_front.mina752.js?ver=4.11.2.1'></script>
</body>

</html>
