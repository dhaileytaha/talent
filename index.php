<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/coconut-talent-logo.png">
    <!-- Title -->
    <title>Coconut Talent </title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="assets/css/owl.carousel.min.css">
    <link href="assets/css/owl.theme.default.min.css">


    

    <!--[if lt IE 9]-->
    <script src="/assets/js/html5shiv.min.js" type="text/javascript"></script>
    <!--[endif]-->
    <script src="/assets/js/owl.carousel.min.js"></script>
    <style>
        .wolf-social .span .wolf-social-1x .hover-none .wolf-social-no-custom-style{
            top:0;
            padding:0;
        }
        /*   .overItem:hover {
           background:
               linear-gradient(
           rgba(255, 0, 0, 0.45),
           rgba(255, 0, 0, 0.45)
           ),
           }*/
    </style>

</head>
<head>
    <?php
    include ('header.php');
    ?>
    <link rel='stylesheet' id='owlcarousel-css'
          href='assets/css/owl.carousel.min001e.css?ver=2.0.0' type='text/css' media='all'/>
    <style>
        .background-image {
            height:auto;
            left:0;
            min-height:100%;
            min-width:1024px;
            position:fixed;
            top:0;
            width:100%;
            z-index: -1
        }

        #dot-matrix {
            background: url(http://s14.directupload.net/images/111129/44ga9qid.png);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 0;
            z-index: 1
        }

        /* This is fancy. not important */

        #content {
            z-index: 9999;
            position: relative;
            margin: auto;
            width: 70%;
            min-height: 600px!important;
        }

        #aboutCoconutTalent {
            margin-top: 60px;
            /*line-height: 5;*/
            /*background: rgba(0, 0, 0, 0.3);*/
            color: white;
            text-shadow: 1px 1px #8DC73F;
            /*color: #8DC73F;*/
            text-align: center;
            font: normal bold 2em/1.1 Calibri;
        }
        #aboutHeader{
            font-family: Balballa;
            margin-top:30px;
            /*line-height: 5;*/
            /* height: 20vh;*/
            font-weight: 100;
            /*background: rgba(0, 0, 0, 0.3);*/
            color: white;
            text-shadow: 4px 1px #8DC73F;
            text-align: center;
            /*color: #8DC73F;*/
            font-size: 38px!important;
        }
       /* h2.background {
            font: 33px sans-serif;
            margin-top: 30px;
            text-align: center;
            text-transform: uppercase;
            position: relative;
            z-index: 1;

        &:before {
             border-top: 2px solid #dfdfdf;
             content:"";
             margin: 0 auto; !* this centers the line to the full width specified *!
             position: absolute; !* positioning must be absolute here, and relative positioning must be applied to the parent *!
             top: 50%; left: 0; right: 0; bottom: 0;
             width: 95%;
             z-index: -1;
         }

        #labelSection {
            !* to hide the lines from behind the text, you have to set the background color the same as the container *!
            background: #fff;
            padding: 0 15px;
        }
        }

        h2.double:before {
            !* this is just to undo the :before styling from above *!
            border-top: none;
        }

        h2.double:after {
            border-bottom: 1px solid black;
            -webkit-box-shadow: 0 1px 0 0 black;
            -moz-box-shadow: 0 1px 0 0 black;
            box-shadow: 0 1px 0 0 black;
            content: "";
            margin: 0 auto; !* this centers the line to the full width specified *!
            position: absolute;
            top: 45%; left: 0; right: 0;
            width: 95%;
            z-index: -1;
        }*/
.modal-backdrop {
z-index: -1 !important;
}
        .vertical-alignment-helper {
            display:table;
            height: 100%;
            width: 100%;
        }
        .vertical-align-center {
            /* To center vertically */
            display: table-cell;
            vertical-align: middle;
        }
        .modal-content {
            /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
            width:inherit;
            height:inherit;
            /* To center horizontally */
            margin: 0 auto;
        }
    </style>
</head>
<body class="home page page-id-6 page-template page-template-page-templates page-template-home page-template-page-templateshome-php wolf wolf-mailchimp tattoopro wolf-woocommerce-sidebar wide-layout skin-light menu-transparent menu-logo-centered menu-dark menu-logo-overflow menu-hover-default no-secondary-menu page-header-big show-title-area is-vc-page full-window-header is-sticky-menu no-top-bar home-header-video is-theme-home is-home-header wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive">

<div id="top"></div>
<a id="top-arrow" class="scroll" href="#top"></a>
<div id="loading-overlay">
    <div id="loader">
        <div class="loader8">
            <div class="loader8-container loader8-container1">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container2">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container3">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
        </div>
    </div>
</div>
<?php
include('nav.php');
?>

<div class="site-container">
    <div id="page" class="hfeed site pusher">
        <div id="page-content">

            <header id="masthead" class="site-header clearfix" role="banner">
                <div class="header-overlay"></div>
                <div class="header-inner">
                    <div class="parallax-inner">
                        <div class="video-container">
                            <div class='video-bg-container'>
                                <div class='video-bg-fallback'
                                     style='background-image:url(../../assets.cdn.wolfthemes.com/tf/tattoopro/tattoopro.jpg)'></div>
                                <video class="video-bg" preload="auto" autoplay loop="loop" muted volume="0">
                                    <source src="assets/img/Ballet Aria Alekzander 2013.mp4"
                                            type="video/mp4">
                                </video>
                                <div class="video-bg-overlay"></div>
                            </div>
                        </div>
                    </div><!-- .parallax-inner -->
                    <div id="hero">
                        <div id="hero-content" class="content-light-font">
                            <div class="wrap">
                                <p><img class="aligncenter size-full" src="" alt=""/></p>
                                <h2 class="videoHeader">Galaxies to Transpire, Art to Stun<br><hr class="horizontal"><span class="headerSub"> Spinning Performance to Inspire & Entertain</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <a id="scroll-down" class="scroll" href="#main"></a>
            </header><!-- #masthead -->

            <div id="main" class="site-main clearfix">
                <div class="site-wrapper">
                    <div id="primary" class="content-area">
                        <main id="vc-content" class="site-content" role="main">
                            <div id="post-6" class="post-6 page type-page status-publish hentry no-thumbnail text-only">
                                <div class="entry-content">
                                    <div style="background-position:center center;background-repeat:no-repeat;-webkit-background-size: 100%; -o-background-size: 100%; -moz-background-size: 100%; background-size: 100%;-webkit-background-size: cover; -o-background-size: cover; background-size: cover;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-standard-width ">
                                        <div class='wolf-row-inner' style=''>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <section class='holder clearfix holder-2-cols wow fadeInLeft'>
                                                            <!--<div class='holder-element holder-content-text content-light-font'>-->
                                                            <!-- <div class="holder-element-inner">-->
                                                            <!--<div class="holder-text-element-inner">-->
                                                            <!--<div class="">-->
                                                            <div id="content">
                                                                <h3 id="aboutHeader">ABOUT COCONUT TALENT</h3>
                                                                <p id="aboutCoconutTalent">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquid aperiam asperiores atque blanditiis consequuntur cumque dolore, doloremque dolores ducimus, ex facere facilis laborum libero nesciunt, non placeat possimus ratione repellat reprehenderit sequi tenetur ut voluptatibus. Assumenda corporis cum deserunt dignissimos distinctio dolor ducimus fuga, hic magni quae veniam veritatis.</p>
                                                            </div>
                                                            <div id="dot-matrix"></div>
                                                            <img class="background-image" src="assets/img/breakdance.jpg">
                                                            <!--</div>-->
                                                            <!--</div>-->
                                                            <!--</div>-->
                                                            <!--</div>-->
                                                        </section>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-position:center center;background-repeat:repeat;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-standard-width ">
                                        <div class='wolf-row-inner' style='padding-top:25px;padding-bottom:50px;'>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  wow fadeIn">
                                                            <div class="wpb_wrapper">
                                                                <h2 style="text-align: center;" id="background"><span id="line">Artists</span></h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row--><!--
                                    <div class="owl-carousel">
                                        <div class="item overItem"><img src="assets/img/image4.jpg"></div>
                                        <div class="item"><img src="assets/img/7.jpg"></div>
                                        <div class="item"><img src="assets/img/image2.jpg"></div>
                                        <div class="item"><img src="assets/img/1.jpg"></div>
                                        <div class="item"><img src="assets/img/2.jpg"></div>
                                        <div class="item"><img src="assets/img/3.jpg"></div>
                                        <div class="item"><img src="assets/img/4.jpg"></div>
                                        <div class="item"><img src="assets/img/5.jpg"></div>
                                        <div class="item"><img src="assets/img/6.jpg"></div>
                                        <div class="item"><img src="assets/img/image2.jpg"></div>
                                        <div class="item"><img src="assets/img/keerthi.jpg"></div>
                                        <div class="item"><img src="assets/img/image4.jpg"></div>
                                    </div>-->
                                    <div style="background-position:center center;background-repeat:no-repeat;-webkit-background-size: 100%; -o-background-size: 100%; -moz-background-size: 100%; background-size: 100%;-webkit-background-size: cover; -o-background-size: cover; background-size: cover;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-standard-width ">
                                        <div class='wolf-row-inner' style=''>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <section class='holder clearfix holder-3-cols owl-carousel'>
                                                            <div class='holder-element holder-content-media content-dark-font'>
                                                                <div class="holder-element-inner">
                                                                    <div class='wolf-linked-image text-center default item'>
                                                                       
                                                                        <img src='assets/img/2.jpg'
                                                                             alt='' class=''><span
                                                                            class='wolf-linked-image-overlay'
                                                                            style='opacity:0;background-color:'></span><span
                                                                            class='wolf-linked-image-caption-container'><span
                                                                                class='wolf-linked-image-caption-table'>
		<span class='wolf-linked-image-caption-table-cell box'>
             <a href="portfolio.php"><div class="overbox">
		<h3 class='wolf-linked-image-caption text-center title overtext col-lg-4' style='color:#fff'>Alex Russo</h3>
		    <span class='wolf-linked-image-secondary-text text-center portfolio tagline overtext'
                  style='color: white !important;'>View Portfolio</span>
		    </div></a>
        </span>
		</span>
		</span></div>
                                                                </div>
                                                            </div>
                                                            <div class='holder-element holder-content-media content-dark-font'>
                                                                <div class="holder-element-inner">
                                                                    <div class='wolf-linked-image text-center default item'>
                                                                    
                                                                        <img src='assets/img/4.jpg'
                                                                             alt='' class=''><span
                                                                            class='wolf-linked-image-overlay'
                                                                            style='opacity:0;background-color:'></span><span
                                                                            class='wolf-linked-image-caption-container'><span
                                                                                class='wolf-linked-image-caption-table'>
        <span class='wolf-linked-image-caption-table-cell box'>
		<a href="portfolio.php"><div class="overbox">
            <h3 class='wolf-linked-image-caption text-center title overtext col-lg-4' style='color:#fff'>Dwayne Johnson</h3>
		<span class='wolf-linked-image-secondary-text text-center tagline overtext' style='color: white !important; '>View Portfolio</span>
		</div></a>
        </span>
		</span>
		</span></div>
                                                                </div>
                                                            </div>
                                                            <div class='holder-element holder-content-media content-dark-font'>
                                                                <div class="holder-element-inner">
                                                                    <div class='wolf-linked-image text-center default item'>
                                                                    
                                                                        <img src='assets/img/3.jpg' alt='' class='hoverOnImage'>

                                                                        <span
                                                                            class='wolf-linked-image-overlay'
                                                                            style='opacity:0;background-color:'></span><span
                                                                            class='wolf-linked-image-caption-container'><span
                                                                                class='wolf-linked-image-caption-table'>
		<span class='wolf-linked-image-caption-table-cell box'>
           <a href="portfolio.php"> <div class="overbox">
		<h3 class='wolf-linked-image-caption text-center title overtext col-lg-4' style='color:#fff'>Ricky Martin</h3>
		<span class='wolf-linked-image-secondary-text text-center tagline overtext' style='color: white !important;'>View Portfolio</span>
		    </div></a>
        </span>
		</span>
		</span></div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-position:center center;background-repeat:repeat;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-standard-width ">
                                        <div class='wolf-row-inner' style='padding-top:25px;padding-bottom:50px;'>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">

                                                        <div class="wpb_text_column wpb_content_element  wow fadeIn">
                                                            <div class="wpb_wrapper">
                                                                <h2 style="text-align: center;" id="background-artist"><span id="line-artist">Artist Of The Month/Week</span></h2>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-position:center center;background-repeat:no-repeat;-webkit-background-size: 100%; -o-background-size: 100%; -moz-background-size: 100%; background-size: 100%;-webkit-background-size: cover; -o-background-size: cover; background-size: cover;"
                                         class="wpb_row section wolf-row clearfix content-light-font wolf-row-full-width ">
                                        <div class='wolf-row-inner' style='padding-top:0px;padding-bottom:0px;'>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <div class='wolf-images-gallery clearfix hover-scale-greyscale mosaic-gallery'
                                                             id='gallery-424'>
                                                            <div class="slide block">
                                                                <a title='' href='assets/img/4931013686_9dc3300e25_o-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/4931013686_9dc3300e25_o-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/14721778903_758749f300_k-960x480.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/14721778903_758749f300_k-960x480.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/14669041054_93ba7c3581_k-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/14669041054_93ba7c3581_k-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/13058240424_8e83c984c6_k-480x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/13058240424_8e83c984c6_k-480x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/8456653933_281fb0d62f_k-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/8456653933_281fb0d62f_k-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/5579396701_dac2882f7b_o-960x480.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/5579396701_dac2882f7b_o-960x480.jpg' alt=''>
                                                                </a>
                                                            </div><!--.block-->
                                                            <div class="slide block">
                                                                <a title='' href='assets/img/5435758598_449e03e564_o-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/5435758598_449e03e564_o-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/5195201264_8231e306c7_o-960x480.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/5195201264_8231e306c7_o-960x480.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/5504075851_f128e56a64_o-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/5504075851_f128e56a64_o-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/6024501835_d9015edc8e_o-480x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/6024501835_d9015edc8e_o-480x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/4998902666_c34855f068_o-960x960.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/4998902666_c34855f068_o-960x960.jpg' alt=''>
                                                                </a>
                                                                <a title='' href='assets/img/14165371693_9fc969944f_k-960x480.jpg' class='lightbox image-item' rel='gallery-410'>
                                                                    <img src='assets/img/14165371693_9fc969944f_k-960x480.jpg' alt=''>

                                                                </a>
                                                            </div><!--.block-->
                                                        </div><!--.wolf-images-gallery-->
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-position:center center;background-repeat:no-repeat;-webkit-background-size: 100%; -o-background-size: 100%; -moz-background-size: 100%; background-size: 100%;-webkit-background-size: cover; -o-background-size: cover; background-size: cover;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-full-width ">
                                        <div class='wolf-row-inner' style='padding-top:25px;padding-bottom:0px;'>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">

                                                        <div class="wpb_text_column wpb_content_element  wow fadeIn">
                                                            <div class="wpb_wrapper">
                                                                <h2 style="text-align: center;" id="background-event"><span id="line-event">Recent Events</span></h2>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 50px"><span
                                                                class="vc_empty_space_inner"></span></div>
                                                        <section class='last-posts-grid'>
                                                            <article
                                                                class="clearfix post-18 post type-post status-publish format-standard has-post-thumbnail hentry category-news has-media has-thumbnail"
                                                                id="post-18" data-post-id="18">
                                                                <div class="post-square-container">
                                                                    <div class="post-square">
                                                                        <a href="2015/05/31/standard-post/index.php"
                                                                           class="entry-thumbnail entry-link">
                                                                            <img width="960" height="960"
                                                                                 src=""
                                                                                 class="attachment-2x2 size-2x2 wp-post-image"
                                                                                 alt="Pin up"
                                                                                 srcset="assets/img/image4.jpg, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-150x150.jpg 150w, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-80x80.jpg 80w, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-180x180.jpg 180w, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-300x300.jpg 300w, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-600x600.jpg 600w, http://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/8293961091_9c418f70fe_k-360x360.jpg 360w"
                                                                                 sizes="(max-width: 960px) 100vw, 960px"/>					<span
                                                                                class="post-square-caption-container">
						<span class="post-square-caption entry-content">
							<span class="category">News</span>								<h2 class="entry-title">Standard Post</h2>
								<span class="entry-meta">
									<span class="meta"><span class="by-author"><span class="pre-meta">by</span> Lorem ipsum</span></span>
									<span class="meta"><span class="pre-meta">on</span> May 31, 2015</span>
								</span>

													</span><!-- .entry-caption -->
					</span><!-- .entry-caption-container -->
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </article><!-- article -->
                                                            <article
                                                                class="clearfix post-17 post type-post status-publish format-image has-post-thumbnail hentry category-event post_format-post-format-image has-media has-thumbnail"
                                                                id="post-17" data-post-id="17">
                                                                <div class="post-square-container">
                                                                    <div class="post-square">
                                                                        <a href="2015/05/31/image-post/index.php"
                                                                           class="entry-thumbnail entry-link">
                                                                            <img width="960" height="960"
                                                                                 src=""
                                                                                 class="attachment-2x2 size-2x2 wp-post-image"
                                                                                 alt="Sketching"
                                                                                 srcset="assets/img/image12.jpeg"
                                                                                 sizes="(max-width: 960px) 100vw, 960px"/>					<span
                                                                                class="post-square-caption-container">
						<span class="post-square-caption entry-content">
							<span class="category">Event</span>								<h2 class="entry-title">Image Post</h2>
								<span class="entry-meta">
									<span class="meta"><span class="by-author"><span class="pre-meta">by</span> Lorem ipsum</span></span>
									<span class="meta"><span class="pre-meta">on</span> May 31, 2015</span>
								</span>

                        </span><!-- .entry-caption -->
					                                </span><!-- .entry-caption-container -->
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </article><!-- article -->
                                                            <article
                                                                class="clearfix post-16 post type-post status-publish format-gallery has-post-thumbnail hentry category-news post_format-post-format-gallery has-media has-thumbnail"
                                                                id="post-16" data-post-id="16">
                                                                <div class="post-square-container">
                                                                    <div class="post-square">
                                                                        <a href="2015/05/31/gallery-post/index.php"
                                                                           class="entry-thumbnail entry-link">
                                                                            <img width="960" height="960"
                                                                                 src=""
                                                                                 class="attachment-2x2 size-2x2 wp-post-image"
                                                                                 alt="Foot Tattoo"
                                                                                 srcset="assets/img/image2.jpg"
                                                                                 sizes="(max-width: 960px) 100vw, 960px"/>					<span
                                                                                class="post-square-caption-container">
						<span class="post-square-caption entry-content">
							<span class="category">News</span>								<h2 class="entry-title">Gallery Post</h2>
								<span class="entry-meta">
									<span class="meta"><span class="by-author"><span class="pre-meta">by</span> Lorem ipsum</span></span>
									<span class="meta"><span class="pre-meta">on</span> May 31, 2015</span>
								</span>

                        </span><!-- .entry-caption -->
					</span><!-- .entry-caption-container -->
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </article><!-- article -->
                                                            <article
                                                                class="clearfix post-15 post type-post status-publish format-video has-post-thumbnail hentry category-event category-news post_format-post-format-video has-media has-thumbnail is-video"
                                                                id="post-15" data-post-id="15">
                                                                <div class="post-square-container">
                                                                    <div class="post-square">
                                                                        <a href="2015/05/31/video-post/index.php"
                                                                           class="entry-thumbnail entry-link">
                                                                            <img width="360" height="360"
                                                                                 src=""
                                                                                 class="attachment-1x1 size-1x1 wp-post-image"
                                                                                 alt="Video thumb"
                                                                                 srcset="assets/img/image13.jpg"
                                                                                 sizes="(max-width: 360px) 100vw, 360px"/><span
                                                                                class="post-square-caption-container">
						<span class="post-square-caption entry-content">
							<span class="category">Event, News</span><h2 class="entry-title">Video Post</h2>
								<span class="entry-meta">
									<span class="meta"><span class="by-author"><span class="pre-meta">by</span> Lorem ipsum.</span></span>
									<span class="meta"><span class="pre-meta">on</span> May 31, 2015</span>
								</span>
                        </span><!-- .entry-caption -->
					                                </span><!-- .entry-caption-container -->
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </article><!-- article -->
                                                        </section>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div class="wpb_row section wolf-row clearfix content-light-font wolf-row-standard-width wolf-row-video-bg ">
                                        <div class='video-bg-container'>
                                            <video class="video-bg" preload="auto" autoplay loop="loop" muted
                                                   volume="0">
                                                <source src="https://0.s3.envato.com/h264-video-previews/166ecb07-b05a-4a70-aea7-6f0993850289/9903135.mp4"
                                                        type="video/mp4">
                                            </video>
                                            <div class="video-bg-overlay"></div>
                                        </div>
                                        <div class="row-overlay" style="background-color:#000000;opacity:0.8;"></div>
                                        <div class='wolf-row-inner' style=''>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">

                                                        <div class="wpb_text_column wpb_content_element  wow fadeIn">
                                                            <div class="wpb_wrapper">
                                                                <h2 style="text-align: center;">Testimonials</h2>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 50px"><span
                                                                class="vc_empty_space_inner"></span></div>
                                                        <section class='testimonials-container wow fadeIn'>
                                                            <div data-pause-on-hover='false'
                                                                 data-autoplay='false'
                                                                 data-transition='slide'
                                                                 data-slideshow-speed='6000'
                                                                 data-nav-arrows='true'
                                                                 data-nav-bullets='true' class='testimonials-slider'>
                                                                <div class='slide'>
                                                                    <div class='testimonal-container'><span
                                                                            class='testimonial-avatar'><img
                                                                                src='assets/img/123.jpg'
                                                                                alt='testimonial-avatar'></span>
                                                                        <blockquote class='testimonial-content'>Lorem
                                                                            ipsum dolor sit amet, consectetur
                                                                            adipisicing elit. Adipisci alias aut autem
                                                                            corporis cum, dignissimos distinctio est
                                                                            eveniet ex exercitationem nihil pariatur
                                                                            placeat porro provident ratione repellendus
                                                                            reprehenderit voluptatum. Delectus dicta
                                                                            distinctio eligendi excepturi quas quod
                                                                            repellat tempore totam. Aliquid assumenda
                                                                            exercitationem explicabo inventore ipsa
                                                                            necessitatibus nemo nihil officiis
                                                                            sequi?<cite
                                                                                class="testimonial-cite">RachelDan</cite>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                                <div class='slide'>
                                                                    <div class='testimonal-container'><span
                                                                            class='testimonial-avatar'><img
                                                                                src='assets/img/234.jpg'
                                                                                alt='testimonial-avatar'></span>
                                                                        <blockquote class='testimonial-content'>Lorem
                                                                            ipsum dolor sit amet, consectetur
                                                                            adipisicing elit. Accusantium ad architecto
                                                                            aspernatur assumenda at cumque dicta
                                                                            distinctio dolor ducimus esse hic in, iste
                                                                            laudantium libero magni natus necessitatibus
                                                                            nobis numquam, officiis, optio pariatur
                                                                            perferendis porro possimus quidem quisquam
                                                                            rerum sequi similique sint tempore ullam
                                                                            veritatis voluptatem voluptatibus
                                                                            voluptatum. Amet, repellendus!<cite
                                                                                class="testimonial-cite">Rachel</cite>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                                <div class='slide'>
                                                                    <div class='testimonal-container'><span
                                                                            class='testimonial-avatar'><img
                                                                                src='assets/img/345.jpg'
                                                                                alt='testimonial-avatar'></span>
                                                                        <blockquote class='testimonial-content'>Lorem
                                                                            ipsum dolor sit amet, consectetur
                                                                            adipisicing elit. Cumque dolore est harum
                                                                            laborum minima pariatur perferendis quasi
                                                                            recusandae. Animi aut consectetur cupiditate
                                                                            deleniti dolor dolorum, ducimus enim facere
                                                                            fuga illum itaque iusto laboriosam libero
                                                                            molestias necessitatibus, numquam obcaecati
                                                                            perferendis placeat quis repellat
                                                                            repudiandae saepe unde vel veniam voluptates
                                                                            voluptatibus voluptatum?<cite
                                                                                class="testimonial-cite">Martin</cite>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-image:url(assets/img/broken_noise.png);background-position:center top;background-repeat:repeat;"
                                         class="wpb_row section wolf-row clearfix content-light-font wolf-row-standard-width ">
                                        <div class='wolf-row-inner' style=''>
                                            <div class="wrap">
                                                <div class="col-6   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h5>ADDRESS</h5>
                                                                <p>501-A/B, Kotia Nirman,
                                                                    Link Road, Andheri-(W),
                                                                    Mumbai-400053</p>
                                                                <h5>PHONE</h5>
                                                                <p>Phone: (022) 40033357</p>
                                                                <h5>EMAIL</h5>
                                                                <p>contact@coconutmediabox.com</p>

                                                            </div>
                                                        </div>
                                                        <hr style='width:100%;'>
                                                        <div class="">
                                                            <a class="twitter-timeline" data-lang="en" data-width="530" data-height="300" data-dnt="true" href="https://twitter.com/CoconutEvent">Tweets by CoconutEvent</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                            <!--<span
                                                                class="wolf-tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, nihil. <a
                                                                    href="https://twitter.com/search/?q=WordPress"
                                                                    target="_blank"></a><a
                                                                    href="https://twitter.com/search/?q=themeforest"
                                                                    target="_blank"></a></span><br><span
                                                                class="wolf-tweet-time_big"><a
                                                                    href="https://twitter.com/wolf_themes/statuses/727427646741696513"
                                                                    target="_blank">about 4 weeks ago</a>
					                                    <span class="wolf-tweet-separator">|</span> <a href="http://twitter.com/wolf_themes/"
                                                                                                       target="_blank">@Coconut Event</a></span>--></div>
                                                    </div>
                                                </div>

                                                <div class="col-6   wolf-col">
                                                    <div class="wpb_wrapper">

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h4>Enquire</h4>

                                                            </div>
                                                        </div>
                                                        <div role="form" class="wpcf7" id="wpcf7-f9-p6-o1" lang="en-US"
                                                             dir="ltr">
                                                            <div class="screen-reader-response"></div>
                                                            <form action="http://demo.wolfthemes.com/tattoopro/#wpcf7-f9-p6-o1"
                                                                  method="post" class="wpcf7-form"
                                                                  novalidate="novalidate">
                                                                <div style="display: none;">
                                                                    <input type="hidden" name="_wpcf7" value="9"/>
                                                                    <input type="hidden" name="_wpcf7_version"
                                                                           value="4.4.1"/>
                                                                    <input type="hidden" name="_wpcf7_locale"
                                                                           value="en_US"/>
                                                                    <input type="hidden" name="_wpcf7_unit_tag"
                                                                           value="wpcf7-f9-p6-o1"/>
                                                                    <input type="hidden" name="_wpnonce"
                                                                           value="a712ed537d"/>
                                                                </div>
                                                                <p>Your Name (required)<br/>
                                                                    <span class="wpcf7-form-control-wrap your-name"><input
                                                                            type="text" name="your-name" value=""
                                                                            size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                            aria-required="true" aria-invalid="false"/></span>
                                                                </p>
                                                                <p>Your Email (required)<br/>
                                                                    <span class="wpcf7-form-control-wrap your-email"><input
                                                                            type="email" name="your-email" value=""
                                                                            size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                            aria-required="true" aria-invalid="false"/></span>
                                                                </p>
                                                                <p>
                                                                    Your phone number<br/>
                                                                    <span class="wpcf7-form-control-wrap tel-155"><input
                                                                            type="tel" name="tel-155" value="" size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                            aria-required="true" aria-invalid="false"/></span>
                                                                </p>
                                                                <p>Subject<br/>
                                                                    <span class="wpcf7-form-control-wrap menu-437"><select
                                                                            name="menu-437"
                                                                            class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required"
                                                                            aria-required="true" aria-invalid="false"><option
                                                                                value="Schedule Consult">Schedule Consult</option><option
                                                                                value="Request Artist">Request Artist</option><option
                                                                                value="Other">Other</option></select></span>
                                                                </p>
                                                                <p>Your Message<br/>
                                                                    <span class="wpcf7-form-control-wrap your-message"><textarea
                                                                            name="your-message" cols="40" rows="10"
                                                                            class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                            aria-required="true"
                                                                            aria-invalid="false"></textarea></span></p>
                                                                <p><input type="submit" value="Send message"
                                                                          class="wpcf7-form-control wpcf7-submit"/></p>
                                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                    <div style="background-image:url(assets/img/image13.jpg);background-position:center center;background-repeat:no-repeat;-webkit-background-size: 100%; -o-background-size: 100%; -moz-background-size: 100%; background-size: 100%;-webkit-background-size: cover; -o-background-size: cover; background-size: cover;"
                                         class="wpb_row section wolf-row clearfix content-dark-font wolf-row-standard-width  section-parallax">
                                        <div class="row-overlay" style="background-color:#000000;opacity:0.6;"></div>
                                        <div class='wolf-row-inner' style=''>
                                            <div class="wrap">
                                                <div class="col-12   wolf-col">
                                                    <div class="wpb_wrapper">
                                                        <div class='wolf-single-image text-center hover-none default wow fadeIn'>
                                                            <span class='image-item'><img
                                                                    src='assets/img/coconut-talent-logo.png'
                                                                    style="height: 300px"
                                                                    alt='' style='width:450px' class=''></span></div>
                                                        <div class='buttons-container wow fadeInUp text-center'>
                                                        	<span class='button-container'>
                                                        		<a data-target="#subscribe" id="subscribeBtn" data-toggle="modal" class='wolf-button large square icon_before accent-color' href='' target='_self'>
                                                        			<i class='fa fa-book'></i> subscribe now
                                                        		</a>
                                                        	</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--.wrap-->
                                        </div><!--.wolf-row-inner-->
                                    </div><!--.wolf-row-->
                                </div><!-- .entry-content -->

                            </div><!-- #post -->


                        </main><!-- main#content .site-content-->
                    </div><!-- #primary .content-area -->
                </div><!-- .site-wrapper -->
            </div><!-- #main -->

        </div><!-- #page-container -->
        <!-- Subscription Modal -->
        <div class="modal fade"  id="subscribe" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Stay In Touch</h4>
                        </div>
                        <div class="modal-body"><input type="text" class="form-control" placeholder="Please Enter Email"> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="subscribeMail">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include('footer.php');
        ?>


    </div><!-- #page .hfeed .site -->
</div><!-- .site-container -->
<script>
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })
</script>
<script>
	$('#subscribeMail').click(function(){
		$('#subscribeBtn').hide();
	})
</script>
<script type='text/javascript'
        src='assets/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript' src='assets/js/scriptsa94e.js?ver=4.4.1'></script>
<script type='text/javascript' src='assets/js/instagram370e.js?ver=1.4.3'></script>
<script type='text/javascript'
        src='assets/js/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='assets/js/woocommerce.min4468.js?ver=2.5.5'></script>
<script type='text/javascript'
        src='assets/js/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'
        src='assets/js/cart-fragments.min4468.js?ver=2.5.5'></script>
<script type='text/javascript' src='assets/js/mediaelement-and-player.min7796.js?ver=2.18.1-a'></script>
<script type='text/javascript' src='assets/js/wp-mediaelement.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/jquery.swipebox.minecc3.js?ver=1.3.0.2'></script>
<script type='text/javascript' src='assets/js/isotope.pkgd.min7406.js?ver=2.0.1'></script>
<script type='text/javascript' src='assets/js/imagesloaded.pkgd.min3a79.js?ver=3.1.8'></script>
<script type='text/javascript' src='assets/js/wow.minf269.js?ver=1.0.1'></script>
<script type='text/javascript' src='assets/js/waypoints.mina752.js?ver=4.11.2.1'></script>
<script type='text/javascript' src='assets/js/jquery.flexslider.min605a.js?ver=2.2.2'></script>
<script type='text/javascript' src='assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.haParallax8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.counterup.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.memo.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.fittext62ea.js?ver=1.2'></script>
<script type='text/javascript' src='assets/js/jquery.wolfSlider2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.carousels2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.likesnviews2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/youtube-video-bg2846.js?ver=1.5.5'></script>

<script type='text/javascript'>
    /* <![CDATA[ */
    var WolfThemeParams = {
        "ajaxUrl": "http://demo.wolfthemes.com/tattoopro/wp-admin/admin-ajax.php",
        "siteUrl": "http://demo.wolfthemes.com/tattoopro/",
        "accentColor": "",
        "headerPercent": "85",
        "breakPoint": "1140",
        "lightbox": "swipebox",
        "videoLightbox": null,
        "footerUncover": null,
        "headerUncover": null,
        "sliderEffect": null,
        "sliderAutoplay": null,
        "sliderSpeed": null,
        "sliderPause": null,
        "infiniteScroll": "true",
        "infiniteScrollMsg": "Loading...",
        "infiniteScrollEndMsg": "No more post to load",
        "loadMoreMsg": "Load More",
        "infiniteScrollEmptyLoad": "assets/img/empty.gif",
        "newsletterPlaceholder": "Your email",
        "isHomeSlider": null,
        "heroFadeWhileScroll": "true",
        "heroParallax": "1",
        "homeHeaderType": "video",
        "isHome": "1",
        "blogWidth": "boxed",
        "menuPosition": "logo-centered",
        "modernMenu": "",
        "currentPostType": [],
        "enableParallaxOnMobile": null,
        "enableAnimationOnMobile": null,
        "doPageTransition": "1",
        "doBackToTopAnimation": "1",
        "onePageMenu": "",
        "isOnePageOtherPage": "1",
        "isStickyMenu": "true",
        "addMenuType": "side",
        "workType": null,
        "isTopbar": null,
        "menuStyle": "transparent",
        "years": "Years",
        "months": "Months",
        "weeks": "Weeks",
        "days": "Days",
        "hours": "Hours",
        "minutes": "Minutes",
        "seconds": "Seconds",
        "replyTitle": "Share your thoughts",
        "doWoocommerceLightbox": "",
        "leftMenuTransparency": null,
        "layout": "wide",
        "HomeHeaderVideoBgType": "selfhosted"
    };
    /*]]>*/
</script>
<script type='text/javascript' src='assets/js/jquery.functions2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/comment-reply.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/wp-embed.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/js_composer_front.mina752.js?ver=4.11.2.1'></script>
<script src="assets/js/rAF.js"></script>
<script src="assets/js/demo-2.js"></script>
</body>

</html><!-- 
$(document).on('shown.bs.modal', '#subscribe', function (e) {
        $('.modal:not(.first-level-dialog)').addClass('second-level-dialog');
    });

    $(document).on('hide.bs.modal', '#subscribe', function (e) {
        $('.modal').removeClass('second-level-dialog');
    }); -->