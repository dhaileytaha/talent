<?php
/**
 * Created by PhpStorm.
 * User: Red Cherrt 13
 * Date: 08-06-2016
 * Time: 10:58 AM
 */

?>
<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="footer-inner clearfix">

        <section id="tertiary" class="sidebar-footer">
            <div class="sidebar-inner wrap">
                <div class="widget-area">
                    <aside id="wolfgram-widget-2" class="widget wolfgram-widget">
                        <div class="widget-content"><h3 class="widget-title">About Us</h3>
                            <div class="textwidget">Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.
                                Accusantium aut consequatur consequuntur
                                corporis culpa eaque eum exercitationem
                                labore molestiae molestias neque nisi
                                perspiciatis placeat possimus provident, quibusdam quo, sint vel.
                                Lorem ipsum dolor sit.<br><br>
                                Phone: (022) 40033357<br>
                                Email: contact@coconutmediabox.com
                            </div>
                        </div>
                        <!--<div class="widget-content"><h3 class="widget-title">Instagrams</h3>
                            <script type="text/javascript">jQuery(document).ready(function ($) {
                                    $('.swipebox-wolfgram-560').swipebox();
                                });
                            </script>
                            <ul class="wolf-instagram-list">
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/1.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/2.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/3.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/4.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/5.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/6.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/7.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/8.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/9.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/10.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/11.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/12.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/13.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/14.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/15.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                                <li><a rel="wolfgram-widget" class="swipebox-wolfgram-560"
                                       href="#"><img
                                            src="assets/img/16.jpg"
                                            alt="wolfgram-thumbnail"></a></li>
                            </ul>
                        </div>-->
                    </aside>
                    <aside id="text-2" class="widget widget_text">
                        <div class="widget-content"><h3 class="widget-title">Categories</h3>
                            <div class='fb-xfbml-parse-ignore'>
                            <div class="textwidget">
                                <ul class="pull-left" style="font-size: 20px; font-family: Calibri">
                                    <li><a href='category.php'>Actors</a></li>
                                    <li><a href='category.php'>Models</a></li>
                                    <li><a href='category.php'>Singers</a></li>
                                    <li><a href='category.php'>Dancers</a></li>
                                    <li><a href='category.php'>Directors</a></li>
                                </ul>
                            </div>
                            </div>
                        </div>
                    </aside>
                    <aside id="" class="widget widget_last_photos">
                        <div class="widget-content">
                            <h3 class="widget-title">Facebook</h3>
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcoconutevent%2F&tabs=timeline&width=250&height=290&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="250" height="290" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            <div style="clear:both"></div>
                        </div>
                    </aside>
                    <aside id="widget_facebook_page_box-3" class="widget widget_facebook_page_box">
                        <div class="widget-content"><h3 class="widget-title">Address</h3>
                            <div class='fb-page'
                                 data-href='https://www.facebook.com/wolfthemes'
                                 data-width='500'
                                 data-height='257'
                                 data-hide-cover='false'
                                 data-show-facepile='false'
                                 data-show-posts='true'>
                                <img src="/assets/img/mumbai-map.png" style="height: 170px; width: 200px!important;">
                                <div class='fb-xfbml-parse-ignore'>
                                    <blockquote cite=''>
                                        <a href='#'>501-A/B, Kotia Nirman,
                                            Link Road, Andheri-(W),
                                            Mumbai-400053</a></blockquote>
                                </div>
                                <!--<div class='fb-xfbml-parse-ignore'>
                                    <blockquote cite='https://www.facebook.com/wolfthemes'>
                                    <a href='https://www.facebook.com/wolfthemes'>Twitter</a></blockquote>
                                </div>
                                <div class='fb-xfbml-parse-ignore'>
                                    <blockquote cite='https://www.facebook.com/wolfthemes'>
                                    <a href='https://www.facebook.com/wolfthemes'>Instagram</a></blockquote>
                                </div>
                                <div class='fb-xfbml-parse-ignore'>
                                    <blockquote cite='https://www.facebook.com/wolfthemes'>
                                    <a href='https://www.facebook.com/wolfthemes'>Pintrest</a></blockquote>
                                </div>-->
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </section><!-- #tertiary .sidebar-footer -->

        <div class="footer-end wrap">
            <nav id="site-navigation-tertiary" class="wrap navigation tertiary-navigation" role="navigation">
                <div class="menu-bottom-menu-container">
                    <ul id="menu-bottom-menu" class="nav-menu-tertiary inline-list">
                        <li id="menu-item-145"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145 sub-menu-dark menu-item-icon-before">
                            <a href="#"><span>Blogs</span></a></li>
                        <li id="menu-item-142"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-142 sub-menu-dark menu-item-icon-before">
                            <a href="portfolio.php"><span>Artists</span></a></li>
                        <!--<li id="menu-item-144"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144 sub-menu-dark menu-item-icon-before">
                            <a href="shop/index.php"><span>Shop</span></a></li>-->
                        <li id="menu-item-143"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143 sub-menu-dark menu-item-icon-before">
                            <a href="rfp.php"><span>Contact us</span></a></li>
                    </ul>
                </div>
            </nav><!-- #site-navigation-tertiary-->
            <div class='theme-socials-container text-center'><a href='#' title='facebook' target='_blank'
                                                                class='wolf-social-link'><span
                        class='wolf-social fa fa-facebook normal wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                    href='#' title='twitter' target='_blank' class='wolf-social-link'><span
                        class='wolf-social fa fa-twitter normal wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                    href='#' title='instagram' target='_blank' class='wolf-social-link'><span
                        class='wolf-social fa fa-instagram normal wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><!--<a
                    href='#' title='foursquare' target='_blank' class='wolf-social-link'>
                    <span class='wolf-social ti-foursquare normal wolf-social-1x hover-none wolf-social-no-custom-style'></span></a>-->
            </div><!-- .theme-socials-container -->
        </div>
    </div>
    <div class="clear"></div>
    <div class="site-infos text-center clearfix">2016 &copy; Coconut Talent | Powered by Coconut Digital</div>
</footer><!-- footer#colophon .site-footer -->
