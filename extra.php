<?php
/**
 * Created by PhpStorm.
 * User: Red Cherrt 13
 * Date: 16-06-2016
 * Time: 04:11 PM
 */
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/component.css" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,800|Clicker+Script' rel='stylesheet' type='text/css'>
    <!--[if IE]-->
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="container demo-2">
    <div class="content">
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
            <h1 class="main-title">Artist</span></h1>
        </div>
    </div><!-- /container -->
</div>
<script src="assets/js/rAF.js"></script>
<script src="assets/js/demo-2.js"></script>
</body>
</html>
