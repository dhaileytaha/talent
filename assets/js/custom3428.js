(function($) {
	$(function() {
		"use strict";
 // Onload Alert Message Start
 	if( $('.content_alert_dialog_wrapper').length > 0 ){
	 if (( $.cookie('entire_site') == null ) || ( $.cookie('entire_site') == undefined )) {
	  	var dialog 	 = $('.content_alert_dialog_box');
		var exit   	 = dialog.find( '.dialog_box_exit_btn' );
		var enter  	 = dialog.find( '.dialog_box_allow_btn' );
		var overlay     = $( '.content_alert_dialog_overlay' );
		$('.content_alert_dialog_box, .content_alert_dialog_overlay').show();
		$( 'body' ).on( 'click', '.dialog_box_allow_btn', function( evt ){ // Cookie Create
			evt.preventDefault();
			var enter_url = enter.find( 'a' ).attr( 'href' );
			$.cookie('entire_site', 'true', { path: "/", expires : 10});
			if( '#' === enter_url || '' === enter_url ){
				dialog.fadeOut( 450, function(){
					overlay.fadeOut( 500 );
				});	
			}		
		});
		$( 'body' ).on( 'click', '.dialog_box_exit_btn', function( evt ){ // Cookie Create
			evt.preventDefault();
			var exit_url = exit.find( 'a' ).attr( 'href' );
			if( '#' === exit_url || '' === exit_url ){
				window.location.replace( 'http://google.com' );
			} else {
				window.location.replace( exit_url );
			}		
		}); 
	}
}
	// Onload Alert Message End
		//Window load
		var header_height = $('.header_content_wrapper').delay(1500).outerHeight();
		var footer_height = $('.bottom_footer_bar_wrapper').outerHeight();
		var window_height = ( $(window).height() - (  footer_height ) );
		$('.primary-menu').on('click touchend', function(e) {
	      var el = $(this);
	      var link = el.attr('href');
	      window.location = link;
	   });
		$('.gallery a').attr('data-gal','prettyPhoto[wp_gallery]'); // Wp Gallery Light Box
		$('#loader').delay(1000).fadeOut();
		/* Header Slider */
		var slides_container = $('.slides-container');
		function talents_main_header_slider() {
		 	if (slides_container.find(".owl-stage-outer").length) {
	            slides_container.trigger("destroy.owl.carousel");
	            slides_container.find(".main_slider_item").unwrap();
	        }
	        var slider_height = $('.main_header_slider_wrapper').data('windowheight');
	        var autoplay = $('.main_header_slider_wrapper').data('autoplay');
	        var columns = $('.main_header_slider_wrapper').data('columns');
	        $('.main_header_slider_wrapper').css({'top':header_height,'position':'relative'});
	        if ($.browser.msie) {
	        	 var autowidth = false;
	        }else{
	        	 var autowidth = $('.main_header_slider_wrapper').data('autowidth');
		        if( autowidth == true ){
				 	$('.main_header_slider_wrapper .main_slider_item img').css('height', ( $(window).height() - (  footer_height + header_height ) ));
				 	$('.main_header_slider_wrapper').css({'height':( $(window).height() - (  footer_height + header_height ) )});
			 	}
		 	}
			//$('.main_header_slider_wrapper').each(function(){
		  	slides_container.owlCarousel({
	            autoWidth: autowidth,
	            margin: 10,
	            items: columns,
	            autoplay : autowidth,
	            smartSpeed: 1300,
	            loop: true,
	            nav:true,
	            onChanged : talents_kaya_hover_title,
				navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	                responsive: {
	                     0:{
		                	items:1,
		                },
		                600:{
		                    items:2,
		                },
		                768:{
		                    items:2,
		                },
		                1024:{
		                   items:3, 
		                },
		                1200:{
		                
		                },
	                },
	            onInitialized: function() {
	            	$('#loading_image').hide();
	            	if( ( autowidth == true ) || ( slider_height !='1' ) ){
	            		slides_container.css('height', ( $(window).height() - (  footer_height + header_height ) ));
	            	}      	    
	            }
	        });
	   	}
	   	if (slides_container.length) {
	        talents_main_header_slider();
	        $(window).on("resize.destroyhorizontal", function() {
	          setTimeout(talents_main_header_slider, 150);
	        });
	    }
		// End
		function talents_kaya_hover_title(){
			// Main Slider Image Hover
			var hover_title_disable = $('.main_header_slider_wrapper').data('title-hover-disable');
			if( hover_title_disable != '1' ){
				$('.main_slider_item').each(function(){
					$(this).hover(function(){
						$(this).find('.slider_description_wrapper').stop(true, true).animate({'bottom':'0'},250);
					}, function(){ 
						$(this).find  ('.slider_description_wrapper').stop(true, true).animate({'bottom':'-100%'},250);
					});
				});
			}
			//End
		}
		talents_kaya_hover_title();
		var title_height = $('.page_title_wrapper h2').height();
		$('.sub_header_wrapper').css({'margin-top': title_height / 2, 'margin-bottom':title_height});
		var  wpadminbar = $('#wpadminbar').height();
		$('.header_logo_top_bar').css('top',wpadminbar);
		function talents_kaya_prettyphoto(){
			$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal',  deeplinking:false,  });
		}	
		talents_kaya_prettyphoto();
		// Menu Section
		function header_menu_section(){			
			if ($(window).width() <= 1006) {
				$('.mobile_menu_section').removeClass('disable_header_mobile_menu');
			}else{
				$('.mobile_menu_section').addClass('disable_header_mobile_menu');	
			}
		}
		$('.mobile_toggle_menu_icons').click(function(){
				$('.mobile_menu_section').slideToggle(250).toggleClass('active');
			});
		header_menu_section();
		$(window).resize(function() {
		  header_menu_section();
		});
	/* Portfolio Image Sorting */
	if (jQuery().isotope){
		$(window).load(function(){
			$(function (){
				if($.browser.safari){ 
					var isotopeContainer = $('.masonry_blog_gallery');
				} else{
					var isotopeContainer = $('.masonry_blog_gallery, .isotope-container');
				}
				isotopeContainer.isotope({
					masonry:{
						columnWidth:    1,
						isAnimated:     true,
						 easing: '',
					},
				})
			});
		});
	}
	if (jQuery().isotope){
			var tempvar = "all";
			jQuery(window).load(function(){
				jQuery(function (){
					var isotopeContainer = jQuery('.isotope-container'),
					isotopeFilter = jQuery('#filter'),
					isotopeLink = isotopeFilter.find('a');
					if($.browser.safari){ } else{
						isotopeContainer.isotope({
							itemSelector : '.isotope-item',
							layoutMode : 'fitRows',
							filter : '.' +tempvar,
							masonry:  {
								columnWidth: 1,
								isAnimated: true,
								isFitWidth: true,
							},
							onLayout: function() {
					   			$('.isotope li').addClass('isotope-ready');
							},
						});
					}
					isotopeLink.click(function(){
						var selector = jQuery(this).attr('data-category');
						isotopeContainer.isotope({
							filter : '.' + selector,
							itemSelector : '.isotope-item',
							layoutMode : 'fitRows',
							animationEngine : 'best-available',
						});
						isotopeLink.removeClass('active');
						jQuery(this).addClass('active');
						return false;
					});

				});
				jQuery("#filter ul li a").removeClass('active');
				jQuery("#filter ul li."+tempvar+ " a").addClass('active');
			});
		}
		/* Portfolio Single page Slider */
		var slider_class = $(".gallery_horizontal");
	    function talents_kaya_pf_single_page_gallery_slider() {
		    slider_class.find('.horizontal_item img').css('height',($(window).height() - ( header_height + footer_height + 80 )));
		    if (slider_class.find(".owl-stage-outer").length) {
	            slider_class.trigger("destroy.owl.carousel");
	            slider_class.find(".horizontal_item").unwrap();
	        }
	        var columns = $(this).data('column');
			var responsive2_column = ( (columns == '4') || (columns == '5') ) ? '3' :columns;
	        slider_class.owlCarousel({
	            autoWidth: true,
	            margin: 10,
	            items: columns,
	            smartSpeed: 1300,
	            loop: true,
	            nav:true,
	            onChanged : talents_kaya_prettyphoto,
				navText : ['<i class="fa fa-angle-right"></i>','<i class="fa fa-angle-left"></i>'],
	                responsive: {
	                     0:{
		                	items:1,
		                	 autoWidth: true,
		                	 autoheight : true,
		                },
		                480:{
		                    items:2,
		                     autoWidth: true,
		                	 autoheight : true,
		                },
		                768:{
		                    items:4,
							autoWidth: true,
							autoheight : true,
		                },
		                1024:{
							items:3,
							autoWidth: true,
							autoheight : true,
		                },
	                },
	            onInitialized: function() {
	            	$('.single_page_slider_loader').hide();
	            	slider_class.css('height',($(window).height() - ( header_height + footer_height + 80 )));
	                
	            }
	        });
	    }
	    if (slider_class.length) {
	        talents_kaya_pf_single_page_gallery_slider();
	        $(window).on("resize.destroyhorizontal", function() {
	          setTimeout(talents_kaya_pf_single_page_gallery_slider, 150);
	        });
	    }	
	    slider_class.on('mousewheel', '.owl-stage', function (e) {
			if (e.deltaY>0) {
			    slider_class.trigger('next.owl');
			} else {
			    slider_class.trigger('prev.owl');
			}
			e.preventDefault();
		});
		//end
		$('.portfolio_content_info_section').on('click','.pf_details_open', function(){
			$(this).next('.portfolio_content_info_wrapper').slideToggle('slow', function() {
		   		$(this).parent().toggleClass('open_model_info_details');
		   		if (!$(".portfolio_content_info_wrapper").is(":visible")){
		   			$(this).prev('.pf_content_show').find('i').addClass('fa-plus').removeClass('fa-minus');
		   		}else{
		   			$(this).prev('.pf_content_show').find('i').removeClass('fa-plus').addClass('fa-minus');
		   		}
		  	});
		});
		//Single Page Tabs
		function talents_kaya_pf_single_page_tabs(){
			$('.single_page_content_wrapper').each(function(){
				talents_kaya_pf_single_page_gallery_slider();
		      	$(".pf_tab_content").hide(); //Hide all content
		      	$(".pf_tab_list > ul li:first").addClass("tab-active").show();
		      	$(".pf_tab_content:first").stop(true,true).fadeIn(0);
		      	$(".pf_tab_list > ul li").click(function() {
		        	$(".pf_tab_list > ul li").removeClass("tab-active");
		        	$(this).addClass("tab-active");
		        	$(".pf_tab_content").stop(true,true).fadeOut(0);
		        	var activeTab = $(this).find("a").attr("href");
		        	$(activeTab).stop(true,true).fadeIn(800);
		        	setTimeout(talents_kaya_pf_single_page_gallery_slider, 10);
		        	return false;
		      	});
			});
		}
		/* Posts Related Slider */
		function talents_kaya_related_post_slider(){
			$('.related_post_slider').each(function(){
				$(this).owlCarousel({
					nav : false,
					autoPlay : true,
					stopOnHover : true,
					loop :true,
					margin : 20,
					onInitialized: function() {
	                	$('.related_post_slider').show();
	            	},
					responsive: {
						0:{
							items:1,
						},
						480:{
							items:1,
						},
						768:{
							items:2,
						},
						1024:{
							items:3,
						},
					},    
				});
			});
		}
		/* Footer Height */
		function julia_kaya_elements_resize(){
			if ( $(window).width() > 1006 ) {
				var footer_height = $('.footer_widgets').height();
				$('.footer_widgets > div').css('height', footer_height);
			}else{
				var footer_height = $('.footer_widgets').height();
				$('.footer_widgets > div').css('height', 'inherit');	
			}
			// Mid container settings
			var sticky_header = $('.header_top_bar_setion').data('sticky-header');
			var sticky_footer = $('.bottom_footer_bar_wrapper').data('footer-sticky');
			var header_height = $('.header_content_wrapper').delay(1500).outerHeight();
			var footer_height = $('.bottom_footer_bar_wrapper').outerHeight();
			if( sticky_header == 'on' ){ // Header
				$('.header_top_bar_setion').css('position','fixed');
				$('#mid_container_wrapper').css({'padding-top':header_height});
			}
			if( sticky_footer == 'on' ){ //Footer
				$('.bottom_footer_bar_wrapper').css('position','fixed');
				$('#mid_container_wrapper').css({'padding-bottom':footer_height});
			}
			// Portfolio Single Page Height
			//$('.portfolio_left_content_section').css('height',($(window).height() - ( header_height + footer_height + 60 )));
			// Menu Height
		}
		
		//Woocommerce Buttons Quantity
		$('.quantity').each(function(){
			$(this).find('.woo_qty_plus').click(function(e){
				e.preventDefault();
				var currentVal = parseInt( $(this).prev('.woo_items_quantity').val());
				if (!isNaN(currentVal)) {
					$(this).prev('.woo_items_quantity').val(currentVal + 1);
				} else {
					$(this).prev('.woo_items_quantity').val(0);
				}
			});
			// This button will decrement the value till 0
			$(this).find(".woo_qty_minus").click(function(e) {
				e.preventDefault();
				var currentVal = parseInt( $(this).next('.woo_items_quantity').val());
					if (!isNaN(currentVal) && currentVal > 0) {
				$(this).next('.woo_items_quantity').val(currentVal - 1);
				} else {
					$(this).next('.woo_items_quantity').val(0);
				}
			});
		});
		// Cross Sales Products
		function talents_kaya_cross_sells_product_slider(){
			$('.cross-sell.products').each(function(){
					$(this).find(".cross-sells-product-slider").owlCarousel({
			        navigation : false,
			        autoplay : true,
			        stopOnHover : true,
			        pagination  : false,
			        items :4,
			         margin:20,
			        smartSpeed: 1500,
			        onInitialized: function() {
	                    $('.cross-sells-product-slider').show();
	                },
	                responsive: {
	                     0:{
		                	items:1,
		                },
		                480:{
		                    items:2,
		                },
		                768:{
		                    items:2,
		                },
		                1024:{
		                    items:3,
		                },
		                1366:{
		                    items:4,
		                },
	                }
		      	});
			});
		}
		//Related Products
		function talents_kaya_related_product_slider(){
			$('.related.products').each(function(){
				$(this).find(".related-product-slider").owlCarousel({
	                navigation : false,
	                autoplay : true,
	                stopOnHover : true,
	                pagination  : false,
	                items :4,
	                margin:20,
	                responsive: {
		                0:{
		                	items:1,
		                },
		                480:{
		                    items:2,
		                },
		                768:{
		                    items:2,
		                },
		                1024:{
		                    items:3,
		                },
		                1366:{
		                    items:4,
		                },
	            	}
		        });
			});
		}
		// Upsales
		function talents_kaya_upsells_product_slider(){
		$('.upsells.products').each(function(){
			$(this).find(".upsells-product-slider").owlCarousel({
                navigation : false,
                autoplay : true,
                stopOnHover : true,
                pagination  : false,
                items :4,
                margin:20,
                responsive: {
	                0:{
	                	items:1,
	                },
	                480:{
	                    items:2,
	                },
	                768:{
	                    items:2,
	                },
	                1024:{
	                    items:3,
	                },
	                1366:{
	                    items:4,
	                },
            	}
              });
		});
		}
		$('.woocommerce-billing-fields').each(function(){
			$(this).find('h3').nextAll().wrapAll( "<div class='woocommerce-billing-fields-wrapper'></div>" );
		});
		$('.woocommerce-shipping-fields').each(function(){
			$(this).find('h3').nextAll().wrapAll( "<div class='woocommerce-shipping-fields-wrapper'></div>" );
		});
		// Video Player
		$('.video_background_wrapper').each(function(){
			var desc_height = $(this).find('.video_description').height();
			$(this).find('.video_description').css('margin-top',-(desc_height / 2));
			$(".player").mb_YTPlayer();
		})
	// Coming Soon Date
	$('.coming_soon_page_wraaper').each(function(){
			var countdown_date = $(this).find('#coming_soon_date').data('date');
			var date_color = $(this).find('#coming_soon_date').data('date-color');
			$(this).find('#coming_soon_date').css("color:"+date_color);

			$(this).find("#coming_soon_date").countdown(countdown_date, function(event) {
		   	var $this = $(this).html(event.strftime(''
		     + '<div class="count_down_wrapper_border"><div class="countdown_wrapper"><span class="countdown_time" style="color:'+date_color+'">%w</span> Weeks </div></div>'
		     + '<div class="count_down_wrapper_border"><div class="countdown_wrapper"><span class="countdown_time"  style="color:'+date_color+'">%d</span> Days </div></div>'
		     + '<div class="count_down_wrapper_border"><div class="countdown_wrapper"><span class="countdown_time"  style="color:'+date_color+'">%H</span> Hours </div></div>'
		     + '<div class="count_down_wrapper_border"><div class="countdown_wrapper"><span class="countdown_time"  style="color:'+date_color+'">%M</span> Min </div></div>'
		     + '<div class="count_down_wrapper_border"><div class="countdown_wrapper"><span class="countdown_time"  style="color:'+date_color+'">%S</span> Sec </div></div>'));
		});
	});
	$('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
	});
	// Scroll Top
	 $(window).scroll(function(){
	    if ($(this).scrollTop() > 100) {
	        $('.scroll_top').fadeIn();
	    } else {
	        $('.scroll_top').fadeOut();
	    }
	});
	 $('.scroll_top').click(function(){
	    $("html, body").animate({ scrollTop: 0 },  600);
	    return false;
	});
 function kaya_image_hover_details(){
        /* Portfolio */    
        $('.advance_search_wrapper').each(function(){
            $(this).find('ul li').hover(function(){
                $(this).find('.pf_content_wrapper .pf_title_description').stop(true,true).slideToggle(300);    
            });   
        });
    }
function talents_kaya_advanced_search(){
  var data_search = $('.searchbox-wrapper').serialize();
     $('#mid_container_wrapper,.main_header_slider_wrapper, .sub_header_wrapper').stop(true,true).animate({'opacity':'0.5'},300);
    $.ajax({
        type:"POST",
        url :wppath.template_path+'/advance_search.php',
        data : {
            advance_search  : 'advance_search ',
            search_name : $('.searchbox-wrapper').serialize(),
        },
        success:function(data){
          $('.sub_header_wrapper').hide();
          $('#mid_container_wrapper,.main_header_slider_wrapper').stop(true,true).animate({'opacity':'1'},300).html(data);
          kaya_image_hover_details();
        }
    });
}
$('.toggle_search_wrapper input, .toggle_search_wrapper select').bind("keyup change", function(){
    talents_kaya_advanced_search();
    return false;
});
$('#search_submit').click(function(){
    talents_kaya_advanced_search();
    return false;
});
//widget search
function talents_kaya_widget_advanced_search(){
  var data_search = $('.searchbox-wrapper').serialize();
     $('.portfolio_content_wrapper').stop(true,true).animate({'opacity':'0.5'},300);
    $.ajax({
        type:"POST",
        url :wppath.template_path+'/advance_search.php',
        data : {
            advance_search  : 'advance_search ',
            search_name : $('.searchbox-wrapper').serialize(),
        },
        
        success:function(data){
          $('.portfolio_content_wrapper').stop(true,true).animate({'opacity':'1'},300).html(data);
          kaya_image_hover_details();
        }
    });
}
$('.widget_toggle_search_wrapper input, .widget_toggle_search_wrapper select').bind("keyup change", function(){
    talents_kaya_widget_advanced_search();
    return false;
});
$('.widget_toggle_search_wrapper #search_submit').click(function(){
    talents_kaya_widget_advanced_search();
    return false;
});
//End
// Model Short list Data added to ajax call back
var shortlist = (function($) {
  // define object literal container for parameters and methods
  var method = {};
	 method.itemActions = function(button) {
    $(button).on('click', function(e) {
      alert('clicked');
      	var target    = $(this),
          item      = target.closest('.item'),
          itemID    = item.attr('id'),
          itemAction= target.data('action');
      	$.ajax({
	        type: 'POST',
	        url: kaya_ajax_url.ajaxurl,
	        data : {
				action : 'talents_kaya_items_remove_add',
				item_action : itemAction,
				item_id : itemID
			},
	        success: function(data) {
	          method.getItemTotal();
	          log(itemAction + ' item ' + itemID);
	          return false;
	        },
	        error: function(data) {
	        	alert(data);
	          log('error with itemActions function', 'check that "themeDirName" has been correctly set in shortlist.js');
        }
     	});
		if (itemAction === 'remove') {
			item.removeClass('item_selected');
		} else {
			item.addClass('item_selected');
		}
      e.preventDefault();
    });

  }; // end fn
  /* make methods accessible
  -------------------------- */
  return method;

}(jQuery)); // end of shortlist constructor	
	// Footer Height
	function talents_kaya_footer_height(){
		if($('body')[0].scrollHeight > $(window).height()){
		    $('.bottom_footer_bar_wrapper').removeClass("footer_scroll");
		}else{
			$('.bottom_footer_bar_wrapper').addClass("footer_scroll");
		}
	}
	// On hover footer socila icons
		$('.social_share_button').click(function(){
			$(this).prev('ul').toggle();
		});
	//
	$('ul.page-numbers').each(function(){
	$(this).find('.current').parent().addClass('current_page');
	})
	/* Shooping Cart */
	$('.widget_shopping_cart_content .buttons a').removeClass('wc-forward');
	$('.woocommerce .button, #review_form_wrapper .form-submit #submit').not('.wc-forward, .shop_product_slider_wrapper .button').addClass('primary-button');
	$('.checkout-button, #place_order, .cart-sussess-message a').addClass('seconadry-button');
	$('.related.products li, .upsells.products li, .cross-sells ul.products li').removeClass('first last');
	$('.add_to_wishlist').removeClass('single_add_to_wishlist button alt primary-button');
	$('i.icon-align-right').removeClass('icon-align-right').addClass('fa fa-heart');
	$('.widget_shopping_cart_content .buttons a').addClass('primary-button');
	$('.cart-sussess-message a').removeClass('seconadry-button');
	// Page title line height
	var title_line_height = $('.title_border_bottom_line').data('line-height');
 	$('span.title_border_bottom_line').delay( 800 ).animate({'height':title_line_height, 'opacity':'1'}, 1000);
	// Resize Function
	julia_kaya_elements_resize();
	//julia_kaya_pf_left_section();
	talents_kaya_footer_height();
	// Detect IE Browser
	if ($.browser.msie) {
	 	$('.page_title_wrapper h2, .page_title_wrapper h2 span').css('opacity','1').addClass('ie_opacity_title');
	 	$('input[type=text], textarea').placeholder();
	 }
// Search 
	$('.toggle_search_icon').click(function(){
		
		//$('body').append('<div class="search_box_overlay"></div>');
		$('.toggle_search_wrapper').css({'display':'block'}).animate({  'opacity':'1','right':'0%'}, 400, 'swing', function(){
		});
	});
	$('.toggle_search_wrapper').each(function(){
		$(this).find('span.search_close').click(function(){
		   $(this).parent().parent('.toggle_search_wrapper').css({'display':'block'}).animate({'opacity':'0','right':'-100%'}, 400, 'swing', function(){		       
		    });
		   //$('.search_box_overlay').remove();
		});
	});
	//Email Sending Validator
jQuery("#send_mail_to_post").click(function(){
  var user_post_link = jQuery("#user_post_link").val();
  var user_email = jQuery("#user_email").val();
  var user_name = jQuery("#user_name").val();
  var receiver_email = jQuery("#receiver_email").val();
  var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
  var hasError = false;
  if( user_email =='' ){
    jQuery('#user_email').addClass('vaidate_error');
     hasError = true;
  }else{
    if (!pattern.test(user_email)) {
    jQuery('#user_email').addClass('vaidate_error');
     hasError = true;
   }else{
     jQuery('#user_email').removeClass('vaidate_error');
     }
   }
  if( user_name == '' ){
	jQuery('#user_name').addClass('vaidate_error')
	}else{
	jQuery('#user_name').removeClass('vaidate_error')
  }
  if( receiver_email =='' ){
		jQuery('#receiver_email').addClass('vaidate_error');
		}else{
			if (!pattern.test(receiver_email)) {
				jQuery('#receiver_email').addClass('vaidate_error');
				hasError = true;
			}else{
				jQuery('#receiver_email').removeClass('vaidate_error');
		}
   }
  if(hasError) { return; }
  else{ 
		jQuery('.mail_form_load_img').show();
		jQuery('#send_mail_to_post').attr('disabled','disable').addClass('button_disable');
		jQuery.ajax({
		type: 'POST',
		url: kaya_ajax_url.ajaxurl,
		data:{
		action: 'talents_kaya_share_post_email',
			'user_email': user_email,
			'user_name' : user_name,
			'receiver_email' : receiver_email,
			'user_post_link':user_post_link,
		},
		success: function(data) {
			jQuery('#send_mail_to_post').removeAttr('disabled').removeClass('button_disable');
			jQuery('.mail_form_load_img').hide();
			jQuery('.success_result').show();
			jQuery('.success_result').html(data.toString());
			return false;
		},
		error: function(data){
			$('p.success_result').html(data.toString())
			return false;
		}
		});
  }
});
//Share ICons
	$('.user_send_email_post').hide();
	$('.pf_social_share_icons').delegate( ".user_email_form", "click",function(){
	  $('.user_send_email_post').show();
	  $(this).addClass('hide_user_form');
	  $('.hide_user_form').removeClass('user_email_form');
	  return false;
	});
	//
	$('.portfolio_content_info_section').delegate( ".hide_user_form, .user_email_form_close", "click",function(){
	  $('.user_send_email_post').hide();
	  $(this).addClass('show_user_form');
	  $('.show_user_form').addClass('user_email_form');
	  $('.user_email_form').removeClass('hide_user_form');
	  return false;
	});
	//
	$(window).load(function() {
		//Scroller
		$('.tab_content_wrapper_loader').hide();
		$('.pf_tabs_content_wrapper').show();
		talents_kaya_pf_single_page_tabs();
		talents_kaya_pf_single_page_gallery_slider();
		talents_kaya_related_post_slider();
		talents_kaya_cross_sells_product_slider();
		talents_kaya_related_product_slider();
		talents_kaya_upsells_product_slider();	
		talents_kaya_footer_height();
		talents_main_header_slider();
	})
	$(window).resize(function() {
		var header_height = $('.header_content_wrapper').outerHeight();
		var footer_height = $('.bottom_footer_bar_wrapper').outerHeight();
		talents_kaya_footer_height();
	  	julia_kaya_elements_resize();
	});	 
	});
})(jQuery);