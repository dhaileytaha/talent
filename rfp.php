<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/coconut-talent-logo.png">
    <!-- Title -->
    <title>Coconut Talent </title>
    <link href="assets/css/theme.css" rel="stylesheet"><!-- Main Template Styles -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/freelancer.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]-->
    <script src="assets/js/html5shiv.min.js" type="text/javascript"></script>
    <!--[endif]-->
    <style>
        .menu-dark #navbar-container a{
            color: green!important;
        }
        body{
            background-color: white!important;
        }
    </style>
</head>
<head>
    <?php
    include ('header.php');
    ?>
</head>
<body class="skin-light menu-transparent menu-logo-centered menu-dark menu-logo-overflow menu-hover-default is-home-header wpb-js-composer">

<div id="top"></div>
<a id="top-arrow" class="scroll" href="#top"></a>
<div id="loading-overlay">
    <div id="loader">
        <div class="loader8">
            <div class="loader8-container loader8-container1">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container2">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
            <div class="loader8-container loader8-container3">
                <div class="loader8-circle1"></div>
                <div class="loader8-circle2"></div>
                <div class="loader8-circle3"></div>
                <div class="loader8-circle4"></div>
            </div>
        </div>
    </div>
</div>
<?php
include('nav.php');
?>

<div class="top hidden-xs hidden-sm">
</div>
<div class="belowTop2">
</div>
<div class="site-container">
    <div id="page" class="hfeed site pusher">
        <div id="page-content">
           <div class="belowTop">
            </div>
            <div class="site-container">
                <div id="page" class="hfeed site pusher">
                    <div id="page-content">
                        <!--<div class="rfp text-center">
                            <h3>Request For Proposal</h3>
                            <hr class="rfpLine">
                            <p class="rfpDetails">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, esse eum fugiat illum, in magnam neque nesciunt obcaecati perferendis perspiciatis possimus quasi sed suscipit tenetur ut. Aut consequuntur culpa dicta, distinctio eius, est fuga fugiat incidunt laudantium, magnam minima minus modi nam natus odit perferendis perspiciatis ratione similique ullam voluptate? Accusantium alias amet aperiam consectetur debitis delectus eligendi eos esse excepturi hic, illo iste non omnis provident quas qui quibusdam quo repellat repudiandae sed sequi sit tenetur ullam velit veniam. Amet eum harum natus quam quod sit sunt veniam veritatis, voluptate voluptatum. Consectetur dolore eum mollitia nam rerum totam vero!</p>

                        </div>-->
                        <div class="row">
                            <div class="col-lg-12">
                                <br>
                               <!-- <div class="col-lg-3">
                                    <div class="lefthanddiv">
                                        <h4>Lorem ipsum dolor sit.</h4>
                                        <hr>
                                        <p>Lorem ipsum dolor.</p>
                                        <ul  style="list-style-type:none">
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                            <li>Lorem ipsum dolor.</li>
                                        </ul>
                                    </div>
                                </div>-->
                                <div class="col-lg-12">
                                    <h2 class="text-center">Contact Us</h2>
                                    <hr class="star-primary">
                                    <div class="col-lg-2"></div>
                                    <div class="text-center col-lg-7">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">First Name :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="fname" placeholder="First Name">
                                            </div>
                                            <label class="control-label col-sm-2" for="email">Last Name :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Mobile :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="" placeholder="Mobile">
                                            </div>
                                            <label class="control-label col-sm-2" for="email">Email:</label>
                                            <div class="col-sm-4">
                                                <input type="email" class="form-control" id="email" placeholder="Enter email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">Address :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="">City :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="" placeholder="City">
                                            </div>
                                            <label class="control-label col-sm-2" for="">Country :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="" placeholder="Country">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success btn-lg pull-right">Contact</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--lefthand Div-->
                </div><!-- .site-wrapper -->
            </div><!-- #main -->
        </div><!-- #page-container -->
        </div><!-- .site-wrapper -->
    </div><!-- #main -->
</div><!-- #page-container -->

<?php
include('footer.php');

?>


    </div><!-- #page .hfeed .site -->
</div><!-- .site-container -->

<script type='text/javascript'
        src='assets/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript' src='assets/js/scriptsa94e.js?ver=4.4.1'></script>
<script type='text/javascript' src='assets/js/instagram370e.js?ver=1.4.3'></script>
<script type='text/javascript'
        src='assets/js/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='assets/js/woocommerce.min4468.js?ver=2.5.5'></script>
<script type='text/javascript'
        src='assets/js/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'
        src='assets/js/cart-fragments.min4468.js?ver=2.5.5'></script>
<script type='text/javascript' src='assets/js/mediaelement-and-player.min7796.js?ver=2.18.1-a'></script>
<script type='text/javascript' src='assets/js/wp-mediaelement.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/jquery.swipebox.minecc3.js?ver=1.3.0.2'></script>
<script type='text/javascript' src='assets/js/isotope.pkgd.min7406.js?ver=2.0.1'></script>
<script type='text/javascript' src='assets/js/imagesloaded.pkgd.min3a79.js?ver=3.1.8'></script>
<script type='text/javascript' src='assets/js/wow.minf269.js?ver=1.0.1'></script>
<script type='text/javascript' src='assets/js/waypoints.mina752.js?ver=4.11.2.1'></script>
<script type='text/javascript' src='assets/js/jquery.flexslider.min605a.js?ver=2.2.2'></script>
<script type='text/javascript' src='assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.haParallax8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='assets/js/jquery.counterup.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.memo.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='assets/js/jquery.fittext62ea.js?ver=1.2'></script>
<script type='text/javascript' src='assets/js/jquery.wolfSlider2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.carousels2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/jquery.likesnviews2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/youtube-video-bg2846.js?ver=1.5.5'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var WolfThemeParams = {
        "ajaxUrl": "http://demo.wolfthemes.com/tattoopro/wp-admin/admin-ajax.php",
        "siteUrl": "http:\/\/demo.wolfthemes.com\/tattoopro\/",
        "accentColor": "",
        "headerPercent": "85",
        "breakPoint": "1140",
        "lightbox": "swipebox",
        "videoLightbox": null,
        "footerUncover": null,
        "headerUncover": null,
        "sliderEffect": null,
        "sliderAutoplay": null,
        "sliderSpeed": null,
        "sliderPause": null,
        "infiniteScroll": "true",
        "infiniteScrollMsg": "Loading...",
        "infiniteScrollEndMsg": "No more post to load",
        "loadMoreMsg": "Load More",
        "infiniteScrollEmptyLoad": "http://demo.wolfthemes.com/tattoopro/wp-content/themes/tattoopro/images/empty.gif",
        "newsletterPlaceholder": "Your email",
        "isHomeSlider": null,
        "heroFadeWhileScroll": "true",
        "heroParallax": "1",
        "homeHeaderType": "video",
        "isHome": "1",
        "blogWidth": "boxed",
        "menuPosition": "logo-centered",
        "modernMenu": "",
        "currentPostType": [],
        "enableParallaxOnMobile": null,
        "enableAnimationOnMobile": null,
        "doPageTransition": "1",
        "doBackToTopAnimation": "1",
        "onePageMenu": "",
        "onePagePage": "http:\/\/demo.wolfthemes.com\/tattoopro\/",
        "isOnePageOtherPage": "1",
        "isStickyMenu": "true",
        "addMenuType": "side",
        "workType": null,
        "isTopbar": null,
        "menuStyle": "transparent",
        "years": "Years",
        "months": "Months",
        "weeks": "Weeks",
        "days": "Days",
        "hours": "Hours",
        "minutes": "Minutes",
        "seconds": "Seconds",
        "replyTitle": "Share your thoughts",
        "doWoocommerceLightbox": "",
        "leftMenuTransparency": null,
        "layout": "wide",
        "HomeHeaderVideoBgType": "selfhosted"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='assets/js/jquery.functions2846.js?ver=1.5.5'></script>
<script type='text/javascript' src='assets/js/comment-reply.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/wp-embed.min3428.js?ver=4.5.2'></script>
<script type='text/javascript' src='assets/js/js_composer_front.mina752.js?ver=4.11.2.1'></script>
</body>

</html>
