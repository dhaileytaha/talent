<?php
/**
 * Created by PhpStorm.
 * User: Red Cherrt 13
 * Date: 08-06-2016
 * Time: 11:11 AM
 */
?>
<head>
    <style>
    #categorySubmenu {

    }
    #parent {
        display: flex;
    }
    #narrow {
        width: 100px !important;
        /* Just so it's visible */
    }
    #wide {
        flex: 1;
        /* Grow to rest of container */
        /* Just so it's visible */
    }
    .actorInSearch{
        width: 125px!important;
        color: black!important;
    }
    </style>
</head>
<div id="ceiling">
    <div id="navbar-container" class="clearfix">
        <div class="wrap">
            <div id="navbar-left" class="navbar clearfix">
                <nav class="site-navigation-primary navigation main-navigation clearfix" role="navigation">
                    <div class="menu-main-menu-left-container">
                        <ul id="menu-main-menu-left" class="nav-menu">
                            <li id="menu-item-180"
                                class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-has-children menu-item-180 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="index.php">Home</a></span>
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-347"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-347 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="indexfe9e.html?home_header_type=video"><span>Video Background</span></a>
                                    </li>
                                    <li id="menu-item-346"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-346 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="index8f32.html?home_header_type=revslider&amp;header_revslider=slider1"><span>Slider Revolution</span></a>
                                    </li>
                                    <li id="menu-item-345"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-345 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="index577c.html?home_header_type=standard"><span>Image Zoom Effect</span></a>
                                    </li>
                                </ul>-->
                            </li>
                            <li id="menu-item-181"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-181 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="#">Artists</a></span>
                                <ul class="sub-menu" id="categorySubmenu">
                                    <li id="menu-item-356"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Models</span></a>
                                    </li>
                                    <li id="menu-item-355"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Actors</span></a>
                                    </li>
                                    <li id="menu-item-354"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-354 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="category.php"><span>Singers</span></a>
                                    </li>
                                    <li id="menu-item-353"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="category.php"><span>Dancers</span></a>
                                    </li>
                                    <li id="menu-item-353"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="category.php"><span>Directors</span></a>
                                    </li>
                                </ul>
                            </li>
                          <!--  <li id="menu-item-182"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-182 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="">Artist</a></span>-->
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-186"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index1216.html?blog_type=masonry"><span>Masonry</span></a></li>
                                    <li id="menu-item-183"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7b2d.html?blog_type=large"><span>Large</span></a></li>
                                    <li id="menu-item-184"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7bb3.html?blog_type=sidebar"><span>Sidebar</span></a></li>
                                    <li id="menu-item-185"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index4304.html?blog_type=sided"><span>Medium Image</span></a>
                                    </li>
                                    <li id="menu-item-187"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-187 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index7761.html?blog_type=masonry&amp;blog_width=wide&amp;blog_infinite_scroll_trigger="><span>Pinterest Style</span></a>
                                    </li>
                                    <li id="menu-item-188"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-188 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="blog/index1082.html?blog_type=grid"><span>Grid</span></a></li>
                                    <li id="menu-item-189"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-189 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="#"><span>Single Post Layouts</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-190"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-190 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/standard-post/index.php"><span>Post with Sidebar</span></a>
                                            </li>
                                            <li id="menu-item-191"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-191 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/image-post/index.php"><span>Full Width Post</span></a>
                                            </li>
                                            <li id="menu-item-192"
                                                class="menu-item menu-item-type-post_type menu-item-object-post menu-item-192 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="2015/05/31/gallery-post/index.php"><span>Splitted Post</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>-->
                           <!-- </li>-->
                            <li id="menu-item-193"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-193 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="albums/index.php"><span>About</span></a>
                                <!--<ul class="sub-menu">
                                    <li id="menu-item-195"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-195 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/index.php"><span>Vertical</span></a></li>
                                    <li id="menu-item-194"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/indexb958.html?gallery_type=modern"><span>Horizontal</span></a>
                                    </li>
                                    <li id="menu-item-196"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-196 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/indexe548.html?gallery_type=grid&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid</span></a>
                                    </li>
                                    <li id="menu-item-197"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-197 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="albums/index8f9d.html?gallery_type=grid&amp;gallery_width=wide&amp;gallery_cols=4&amp;gallery_padding=no-padding&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid Full Width</span></a>
                                    </li>
                                    <li id="menu-item-198"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-198 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="#"><span>Single Gallery Layouts</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-199"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-199 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/mosaic/index.php"><span>Mosaic</span></a></li>
                                            <li id="menu-item-200"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-200 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/masonry/index.php"><span>Masonry</span></a></li>
                                            <li id="menu-item-201"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-201 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/standard-grid/index.php"><span>Standard Grid</span></a>
                                            </li>
                                            <li id="menu-item-202"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-202 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/squares-grid/index.php"><span>Squares Grid</span></a>
                                            </li>
                                            <li id="menu-item-203"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-203 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/4-columns-full-width/index.php"><span>4 Columns Full Width</span></a>
                                            </li>
                                            <li id="menu-item-204"
                                                class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-204 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="gallery/2-columns-centered/index.php"><span>2 Columns Centered</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>-->
                            </li>
                        </ul>
                    </div>
                </nav><!-- #site-navigation-primary -->
            </div><!-- #navbar -->
            <div class="logo"><a href="" rel="home"><img class="logo-dark"
                                                                  src="assets/img/coconut-talent-logo.png"
                                                                  alt=""><img class="logo-light"
                                                                              style="height: 150px;"
                                                                              src="assets/img/coconut-talent-logo.png"
                                                                              alt="Tattoo Pro"></a></div>
            <div id="navbar-right" class="navbar clearfix">
                <nav class="site-navigation-primary navigation main-navigation clearfix" role="navigation">
                    <span class="menu-main-menu-right-container">
                        <ul id="menu-main-menu-right" class="nav-menu">
                            <li id="menu-item-281"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-281 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><span><a href="rfp.php">Contact</a></span>
                               <!-- <ul class="sub-menu">
                                    <li id="menu-item-298"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-298 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Templates</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-299"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="small-width/index.php"><span>Small Width</span></a></li>
                                            <li id="menu-item-300"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="full-width/index.php"><span>Full Width</span></a></li>
                                            <li id="menu-item-301"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="sidebar-at-left/index.php"><span>Sidebar at Left</span></a>
                                            </li>
                                            <li id="menu-item-302"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-302 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="sidebar-at-right/index.php"><span>Sidebar at Right</span></a>
                                            </li>
                                            <li id="menu-item-303"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-303 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="page-with-comments/index.php"><span>Page with Comments</span></a>
                                            </li>
                                            <li id="menu-item-304"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a href="post-archives/index.php"><span>Post Archives</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-305"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-305 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Headers</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-306"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-306 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-parallax/index.php"><span>Header Parallax</span></a>
                                            </li>
                                            <li id="menu-item-307"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-zoom/index.php"><span>Header Zoom</span></a></li>
                                            <li id="menu-item-308"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-video/index.php"><span>Header Video</span></a></li>
                                            <li id="menu-item-309"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-full-screen/index.php"><span>Header Full Screen</span></a>
                                            </li>
                                            <li id="menu-item-310"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-small-height/index.php"><span>Header Small Height</span></a>
                                            </li>
                                            <li id="menu-item-311"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="header-breadcrumb/index.php"><span>Header Breadcrumb</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-312"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-312 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Menu Styles</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-318"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-318 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/index1e9f.html?menu_position=default"><span>Menu Logo at Left</span></a>
                                            </li>
                                            <li id="menu-item-313"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-313 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexc639.html?menu_width=wide&amp;menu_position=default"><span>Menu Wide</span></a>
                                            </li>
                                            <li id="menu-item-314"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-314 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexcd08.html?menu_style=semi-transparent"><span>Menu Semi-Transparent</span></a>
                                            </li>
                                            <li id="menu-item-315"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-315 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/index1974.html?menu_style=plain"><span>Menu Plain</span></a>
                                            </li>
                                            <li id="menu-item-316"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-316 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexccaf.html?menu_skin=light&amp;menu_style=plain"><span>Menu Light</span></a>
                                            </li>
                                            <li id="menu-item-317"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-317 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="menu-styles/indexade1.html?menu_position=left&amp;search_menu_item=0&amp;cart_menu_item=0"><span>Menu at Left</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-319"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-319 not-linked sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Socials</span></a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-320"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a
                                                    href="instagram/index.php"><span>Instagram</span></a></li>
                                            <li id="menu-item-321"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321 sub-menu-dark menu-item-icon-before"
                                                data-mega-menu-bg-repeat='no-repeat'><a href="twitter/index.php"><span>Twitter</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-344"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="faq/index.php"><span>FAQ</span></a></li>
                                    <li id="menu-item-322"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="contact/index.php"><span>Contact</span></a></li>
                                    <li id="menu-item-323"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="maintenance/index.php"><span>Maintenance</span></a>
                                    </li>
                                    <li id="menu-item-324"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-324 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="404"><span>404</span></a></li>
                                </ul>-->
                            </li>
                            <li id="menu-item-282"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>I'm lookin for</span></a>
                                <!-- <ul class="sub-menu">
                                     <li id="menu-item-283"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="parallax/index.php"><span><i
                                             class="fa fa-align-center"></i>Parallax</span></a></li>
                                     <li id="menu-item-284"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="assets/img/index.php"><span><i
                                             class="fa fa-picture-o"></i>Image Galleries</span></a></li>
                                     <li id="menu-item-285"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="image-sliders/index.php"><span><i
                                             class="fa fa-eye"></i>Image Sliders</span></a></li>
                                     <li id="menu-item-286"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-286 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="team-members/index.php"><span><i
                                             class="fa fa-users"></i>Team Members</span></a></li>
                                     <li id="menu-item-287"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="socials/index.php"><span><i
                                             class="fa fa-share-alt"></i>Socials</span></a></li>
                                     <li id="menu-item-288"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-288 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="blocks/index.php"><span><i
                                             class="fa fa-square"></i>Blocks</span></a></li>
                                     <li id="menu-item-289"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="buttons/index.php"><span><i
                                             class="fa fa-square-o"></i>Buttons</span></a></li>
                                     <li id="menu-item-290"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a
                                             href="call-to-action/index.php"><span><i
                                             class="fa fa-exclamation-circle"></i>Call to Action</span></a></li>
                                     <li id="menu-item-291"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="custom-fonts/index.php"><span><i
                                             class="fa fa-font"></i>Custom Fonts</span></a></li>
                                     <li id="menu-item-292"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="counters/index.php"><span><i
                                             class="fa fa-sort-alpha-asc"></i>Counters</span></a></li>
                                     <li id="menu-item-293"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="count-down/index.php"><span><i
                                             class="fa fa-sort-alpha-desc"></i>Count Down</span></a></li>
                                     <li id="menu-item-294"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-294 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="notifications/index.php"><span><i
                                             class="fa fa-exclamation"></i>Notifications</span></a></li>
                                     <li id="menu-item-295"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-295 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a
                                             href="accordions-toggles/index.php"><span><i class="fa fa-toggle-on"></i>Accordions &#038; Toggles</span></a>
                                     </li>
                                     <li id="menu-item-296"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="tabs/index.php"><span><i
                                             class="fa fa-table"></i>Tabs</span></a></li>
                                     <li id="menu-item-297"
                                         class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297 sub-menu-dark menu-item-icon-before"
                                         data-mega-menu-bg-repeat='no-repeat'><a href="columns/index.php"><span><i
                                             class="fa fa-columns"></i>Columns</span></a></li>
                                 </ul>-->
                            </li>
                            <li id="menu-item-282"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Blogs</span></a>
                            </li>
                            <!--<li class="socials-menu-item">-->
                               <!-- <div class='theme-socials-container text-center'><a href='#' title='facebook'
                                                                                    target='_blank'
                                                                                    class='wolf-social-link'><span
                                            class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='twitter' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='instagram' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                        href='#' title='foursquare' target='_blank' class='wolf-social-link'><span
                                            class='wolf-social ti-foursquare span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a>
                                </div>--><!-- .theme-socials-container -->
                            <!--</li>-->
                            <script>
                                $(document).ready(function() {
                                    $('.parent').on("click",function(){

                                        $(this).find(".sub-menu").toggle();
                                        $(this).siblings().find(".sub-menu").hide();

                                        if($(".sub-menu:visible").length === 0 ){
                                            $(".child-menu").hide();
                                        }else {
                                            $(".child-menu").show();
                                        }
                                    });

                                    $(".sub-menu").on("click",function(){
                                        $(".sub-nav").hide();
                                        $(this).hide();
                                    });
                                });
                            </script>
                            <li id="menu-item-22"
                                class="parent menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span class="fa fa-search"></span></a>
                                 <ul class="sub-menu child-menu dropdown-menu mega-dropdown-menu row" style="background-color: whitesmoke !important; margin-top: 0px; width: 600px!important; margin-left: -380px">
                                     <li class="col-sm-5">
                                         <ul class="searchOptions">
                                             <li class="dropdown-header">New Artists</li>
                                             <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                 <div class="carousel-inner">
                                                     <div class="item active">
                                                         <a href="#"><img src="/assets/img/1.jpg" class="img-responsive" alt="product 1"></a>
                                                         <h4><small>Brad Pitt</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">49,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                     <div class="item">
                                                         <a href="#"><img src="/assets/img/2.jpg" class="img-responsive" alt="product 2"></a>
                                                         <h4><small>Josef Peter</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">9,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                     <div class="item">
                                                         <a href="#"><img src="/assets/img/3.jpg" class="img-responsive" alt="product 3"></a>
                                                         <h4><small>Tom Cruise</small></h4>
                                                         <!--<button class="btn btn-primary" type="button">49,99 €</button>-->
                                                         <!--<button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>-->
                                                     </div>
                                                     <!-- End Item -->
                                                 </div>
                                                 <!-- End Carousel Inner -->
                                             </div>
                                             <!-- /.carousel -->
                                             <li class="divider"></li>
                                             <li><a href="category.php" style="color:black!important">View All <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                                         </ul>
                                     </li>
                                     <li class="col-sm-7">
                                         <ul>
                                             <li class="dropdown-header">Search</li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Actors :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Singers :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Models :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Dancers :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li class="checkboxSearch">
                                                 <div id="parent">
                                                     <div class="wide">
                                                        <a href="#" class="actorInSearch" style="color:black!important">Directors :</a>
                                                     </div>
                                                     <div class="narrow">
                                                         <label class="checkbox-inline" style="width: 61px"><input type="checkbox" name="actor" class="">Male</label>
                                                     </div>
                                                     <div>
                                                         <label class="checkbox-inline"><input type="checkbox" value="">Female</label>
                                                     </div>
                                                 </div>
                                             </li>
                                             <li><a href="#">
                                                     <select class="" data-mega-menu-bg-repeat='no-repeat'>
                                                         <option selected disabled>Height</option>
                                                         <option>Less than 3 feet</option>
                                                         <option>3.1'</option>
                                                         <option>3.2'</option>
                                                         <option>3.3'</option>
                                                         <option>3.4'</option>
                                                         <option>3.5'</option>
                                                         <option>3.6'</option>
                                                         <option>3.7'</option>
                                                         <option>3.8'</option>
                                                         <option>3.9'</option>
                                                         <option>3.10'</option>
                                                         <option>3.11'</option>
                                                         <option>3.12'</option>
                                                         <option>4.1'</option>
                                                         <option>4.2'</option>
                                                         <option>4.3'</option>
                                                         <option>4.4'</option>
                                                         <option>4.5'</option>
                                                         <option>4.6'</option>
                                                         <option>4.7'</option>
                                                         <option>4.8'</option>
                                                         <option>4.9'</option>
                                                         <option>4.10'</option>
                                                         <option>4.11'</option>
                                                         <option>4.12'</option>
                                                         <option>5.1'</option>
                                                         <option>5.2'</option>
                                                         <option>5.3'</option>
                                                         <option>5.4'</option>
                                                         <option>5.5'</option>
                                                         <option>5.6'</option>
                                                         <option>5.7'</option>
                                                         <option>5.8'</option>
                                                         <option>5.9'</option>
                                                         <option>5.10'</option>
                                                         <option>5.11'</option>
                                                         <option>5.12'</option>
                                                         <option>Above 6</option>
                                                     </select>
                                                 </a>
                                             </li>
                                             <li class="divider"></li>
                                             <!--<li class="dropdown-header">Tops</li>-->
                                             <li><a href="category.php" class="btn btn-success" role="button"><button class="btn btn-success">Go</button></a></li>
                                         </ul>
                                     </li>
                                     <!--<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                         data-mega-menu-bg-repeat='no-repeat'><input type="search" placeholder="Search" style="width: 270px">
                                     </li>-->
                                     <!--<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                         data-mega-menu-bg-repeat='no-repeat' style="color:white; font-size: 16px">
                                         Category :<br>
                                         Actor Male
                                         <input type="checkbox" name="actor">
                                         Actor Female
                                         <input type="checkbox" name="actor"><br>
                                         Singer Male
                                         <input type="checkbox" name="actor">
                                         Singer Female
                                         <input type="checkbox" name="actor"><br>
                                         Model Male
                                         <input type="checkbox" name="actor">
                                         Model Female
                                         <input type="checkbox" name="actor"><br><br>
                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                  data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Gender</option>
                                             <option>Male</option>
                                             <option>Female</option>
                                         </select>

                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                 data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Height</option>
                                             <option>Less than 3 feet</option>
                                             <option>3.1'</option>
                                             <option>3.2'</option>
                                             <option>3.3'</option>
                                             <option>3.4'</option>
                                             <option>3.5'</option>
                                             <option>3.6'</option>
                                             <option>3.7'</option>
                                             <option>3.8'</option>
                                             <option>3.9'</option>
                                             <option>3.10'</option>
                                             <option>3.11'</option>
                                             <option>3.12'</option>
                                             <option>4.1'</option>
                                             <option>4.2'</option>
                                             <option>4.3'</option>
                                             <option>4.4'</option>
                                             <option>4.5'</option>
                                             <option>4.6'</option>
                                             <option>4.7'</option>
                                             <option>4.8'</option>
                                             <option>4.9'</option>
                                             <option>4.10'</option>
                                             <option>4.11'</option>
                                             <option>4.12'</option>
                                             <option>5.1'</option>
                                             <option>5.2'</option>
                                             <option>5.3'</option>
                                             <option>5.4'</option>
                                             <option>5.5'</option>
                                             <option>5.6'</option>
                                             <option>5.7'</option>
                                             <option>5.8'</option>
                                             <option>5.9'</option>
                                             <option>5.10'</option>
                                             <option>5.11'</option>
                                             <option>5.12'</option>
                                             <option>Above 6</option>
                                         </select>
                                         <br>
                                         <select class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before menu-search"
                                                 data-mega-menu-bg-repeat='no-repeat'>
                                             <option>Age</option>
                                             <option>1</option>
                                             <option>2</option>
                                             <option>3</option>
                                             <option>4</option>
                                             <option>5</option>
                                             <option>6</option>
                                             <option>7</option>
                                             <option>8</option>
                                             <option>9</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>Above 20</option>
                                             <option>Below 35</option>
                                             <option>Above 35</option>
                                         </select>
                                         <br><br>
                                         <button class="btn btn-success pull-right">Go</button><br>
                                     </li>
                                 </ul>
                            </li>-->
                            <!--<li class="menu-item">

                                    <input type="search" placeholder="Search" style="width: 170px">

                            </li>-->
                        </ul>
                            </li>
                            </ul>
                    </div>
                </nav><!-- #site-navigation-primary --> 
            </div><!-- #navbar -->
        </div><!-- .wrap -->
    </div><!-- #navbar-container -->
</div><!--#Ceiling-->

<div id="mobile-bar" class="clearfix">
    <div id="mobile-bar-inner">
        <div id="menu-toggle" class="menu-toggle">
            <div class="burger-before"></div>
            <div class="burger"></div>
            <div class="burger-after"></div>
        </div>
        <div class="logo"><a href="index.php" rel="home"><img class="logo-dark"
                                                              src=""
                                                              alt=""><img class="logo-light"
                                                                                    src=""
                                                                                    alt=""></a></div>
    </div>
</div>
<div id="navbar-mobile-container">
    <div id="navbar-mobile" class="navbar clearfix">
        <!-- <span id="close-menu">&times;</span> -->
        <nav id="site-navigation-primary-mobile" class="navigation main-navigation clearfix" role="navigation">

            <div class="menu-main-menu-left-container">
                <ul id="mobile-menu" class="nav-menu dropdown">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-180 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="index.php"><span>Home</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-347 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="indexfe9e.html?home_header_type=video"><span>Video Background</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-346 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="index8f32.html?home_header_type=revslider&amp;header_revslider=slider1"><span>Slider Revolution</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-345 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="index577c.html?home_header_type=standard"><span>Image Zoom Effect</span></a>
                            </li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-181 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="portfolio.php"><span>Category</span></a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Models</span></a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Actor</span></a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-354 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="#"><span>Singer</span></a></li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Artist</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index1216.html?blog_type=masonry"><span>Masonry</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7b2d.html?blog_type=large"><span>Large</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7bb3.html?blog_type=sidebar"><span>Sidebar</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index4304.html?blog_type=sided"><span>Medium Image</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-187 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="blog/index7761.html?blog_type=masonry&amp;blog_width=wide&amp;blog_infinite_scroll_trigger="><span>Pinterest Style</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-188 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="blog/index1082.html?blog_type=grid"><span>Grid</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-189 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Single Post Layouts</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-190 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="2015/05/31/standard-post/index.php"><span>Post with Sidebar</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-191 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="2015/05/31/image-post/index.php"><span>Full Width Post</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-192 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="2015/05/31/gallery-post/index.php"><span>Splitted Post</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-193 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>About</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-195 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/index.php"><span>Vertical</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/indexb958.html?gallery_type=modern"><span>Horizontal</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-196 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/indexe548.html?gallery_type=grid&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-197 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="albums/index8f9d.html?gallery_type=grid&amp;gallery_width=wide&amp;gallery_cols=4&amp;gallery_padding=no-padding&amp;c=%23page%7B%09margin-top%3A150px%3B%7D%23navbar-container%7B%09background-color%3A%230d0d0d!important%3B%7D"><span>Grid Full Width</span></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-198 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Single Gallery Layouts</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-199 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/mosaic/index.php"><span>Mosaic</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-200 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/masonry/index.php"><span>Masonry</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-201 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="gallery/standard-grid/index.php"><span>Standard Grid</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-202 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/squares-grid/index.php"><span>Squares Grid</span></a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-203 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/4-columns-full-width/index.php"><span>4 Columns Full Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-gallery menu-item-204 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="gallery/2-columns-centered/index.php"><span>2 Columns Centered</span></a>
                                    </li>
                                </ul>-->
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu-main-menu-right-container">
                <ul id="mobile-menu" class="nav-menu dropdown">
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-281 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="contact.php"><span>Contact</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-298 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Templates</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="small-width/index.php"><span>Small Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="full-width/index.php"><span>Full Width</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="sidebar-at-left/index.php"><span>Sidebar at Left</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-302 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="sidebar-at-right/index.php"><span>Sidebar at Right</span></a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-303 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="page-with-comments/index.php"><span>Page with Comments</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="post-archives/index.php"><span>Post Archives</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-305 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Headers</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-306 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-parallax/index.php"><span>Header Parallax</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-zoom/index.php"><span>Header Zoom</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="header-video/index.php"><span>Header Video</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-full-screen/index.php"><span>Header Full Screen</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-small-height/index.php"><span>Header Small Height</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="header-breadcrumb/index.php"><span>Header Breadcrumb</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-312 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Menu Styles</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-318 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/index1e9f.html?menu_position=default"><span>Menu Logo at Left</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-313 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexc639.html?menu_width=wide&amp;menu_position=default"><span>Menu Wide</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-314 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexcd08.html?menu_style=semi-transparent"><span>Menu Semi-Transparent</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-315 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/index1974.html?menu_style=plain"><span>Menu Plain</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-316 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexccaf.html?menu_skin=light&amp;menu_style=plain"><span>Menu Light</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-317 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="menu-styles/indexade1.html?menu_position=left&amp;search_menu_item=0&amp;cart_menu_item=0"><span>Menu at Left</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-319 not-linked sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>Socials</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a href="instagram/index.php"><span>Instagram</span></a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321 sub-menu-dark menu-item-icon-before"
                                        data-mega-menu-bg-repeat='no-repeat'><a
                                            href="twitter/index.php"><span>Twitter</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="faq/index.php"><span>FAQ</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="contact/index.php"><span>Contact</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a
                                    href="maintenance/index.php"><span>Maintenance</span></a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-324 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="404"><span>404</span></a></li>
                        </ul>-->
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-282 not-linked sub-menu-dark menu-item-icon-before"
                        data-mega-menu-bg-repeat='no-repeat'><a href="#"><span>RFP</span></a>
                        <!--<ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="parallax/index.php"><span><i
                                            class="fa fa-align-center"></i>Parallax</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="assets/img/index.php"><span><i
                                            class="fa fa-picture-o"></i>Image Galleries</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="image-sliders/index.php"><span><i
                                            class="fa fa-eye"></i>Image Sliders</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-286 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="team-members/index.php"><span><i
                                            class="fa fa-users"></i>Team Members</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="socials/index.php"><span><i
                                            class="fa fa-share-alt"></i>Socials</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-288 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="blocks/index.php"><span><i
                                            class="fa fa-square"></i>Blocks</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="buttons/index.php"><span><i
                                            class="fa fa-square-o"></i>Buttons</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="call-to-action/index.php"><span><i
                                            class="fa fa-exclamation-circle"></i>Call to Action</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="custom-fonts/index.php"><span><i
                                            class="fa fa-font"></i>Custom Fonts</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="counters/index.php"><span><i
                                            class="fa fa-sort-alpha-asc"></i>Counters</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="count-down/index.php"><span><i
                                            class="fa fa-sort-alpha-desc"></i>Count Down</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-294 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="notifications/index.php"><span><i
                                            class="fa fa-exclamation"></i>Notifications</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-295 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="accordions-toggles/index.php"><span><i
                                            class="fa fa-toggle-on"></i>Accordions &#038; Toggles</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="tabs/index.php"><span><i
                                            class="fa fa-table"></i>Tabs</span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297 sub-menu-dark menu-item-icon-before"
                                data-mega-menu-bg-repeat='no-repeat'><a href="columns/index.php"><span><i
                                            class="fa fa-columns"></i>Columns</span></a></li>
                        </ul>-->
                    </li>
                    <li class="socials-menu-item">

                        <!--<div class='theme-socials-container text-center'><a href='#' title='facebook' target='_blank'
                                                                            class='wolf-social-link'><span
                                    class='wolf-social fa fa-facebook span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='twitter' target='_blank' class='wolf-social-link'><span
                                    class='wolf-social fa fa-twitter span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='instagram' target='_blank' class='wolf-social-link'><span
                                    class='wolf-social fa fa-instagram span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a><a
                                href='#' title='foursquare' target='_blank' class='wolf-social-link'><!--<span
                                    class='wolf-social ti-foursquare span wolf-social-1x hover-none wolf-social-no-custom-style'></span></a>
                        </div>-->
                    </li>
                </ul>
            </div>
        </nav><!-- #site-navigation-primary -->
    </div><!-- #navbar -->
</div>
